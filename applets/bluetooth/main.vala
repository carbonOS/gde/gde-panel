private void update_status(Panel.CCToggle toggle, BtManager mgr) {
    if (mgr.powered && mgr.connected) {
        toggle.icon_name = toggle.panel_icon_name = "bluetooth-active-symbolic";
        toggle.panel_tooltip = "Connected to %s".printf(mgr.connection_name);
        toggle.status = "<small>%s</small>".printf(mgr.connection_name);
    } else if (mgr.powered) {
        toggle.icon_name = "bluetooth-disconnected-symbolic";
        toggle.panel_icon_name = null;
        toggle.panel_tooltip = null;
        toggle.status = "<small>Disconnected</small>";
    } else {
        toggle.icon_name = "bluetooth-disabled-symbolic";
        toggle.panel_icon_name = null;
        toggle.panel_tooltip = toggle.status = null;
    }
}

public Panel.Applet? create_applet() {
    Panel.Signals sig = Panel.Signals.obtain();

    var mgr = new BtManager();

    var toggle = new Panel.CCToggle();
    toggle.name = "Bluetooth";
    toggle.status = null;
    toggle.ordering = 24;
    toggle.more_settings = "gnome-bluetooth-panel.desktop";

    mgr.bind_property("powered", toggle, "active", BindingFlags.SYNC_CREATE);
    mgr.bind_property("available", toggle, "visible", BindingFlags.SYNC_CREATE);

    toggle.notify["active"].connect(() => mgr.set_bt_powered.begin(toggle.active));
    mgr.notify["powered"].connect(() => update_status(toggle, mgr));
    mgr.notify["connected"].connect(() => update_status(toggle, mgr));
    update_status(toggle, mgr);

    sig.register_cc_toggle(toggle);

	return null; // We don't want a regular applet
}
