class BtManager : Object {
    public bool available {
        get; private set;
        default = false;
    }
    public bool powered {
        get; private set;
        default = false;
    }
    public bool connected {
        get; private set;
        default = false;
    }
    public string? connection_name {
        get; private set;
        default = null;
    }

    private DBusObjectManagerClient obj_mgr;

    construct {
        setup_mgr.begin();
    }

    [CCode (cname="bt_device_proxy_get_type")]
    extern static Type get_device_proxy_type();
    [CCode (cname="bt_adapter_proxy_get_type")]
    extern static Type get_adapter_proxy_type();

    private static Type obj_mgr_proxy_get_type(DBusObjectManagerClient mgr,
                                        string obj_path, string? iface_name) {
        if (iface_name == null)
            return typeof(DBusObjectProxy);

        switch (iface_name) {
            case "org.bluez.Device1":
                return get_device_proxy_type();
            case "org.bluez.Adapter1":
                return get_adapter_proxy_type();
            default:
                return typeof(DBusProxy);
        }
    }

    private async void setup_mgr() {
        obj_mgr = yield new DBusObjectManagerClient.for_bus(
            BusType.SYSTEM,
            DBusObjectManagerClientFlags.NONE,
            "org.bluez", "/",
            obj_mgr_proxy_get_type
        );

        obj_mgr.interface_added.connect((obj, iface) => update());
        obj_mgr.interface_removed.connect((obj, iface) => update());
        obj_mgr.object_added.connect((obj, iface) => update());
        obj_mgr.object_removed.connect((obj, iface) => update());
        update();
    }

    // Update on idle
    private void update() {
        Idle.add(() => {
            _update();
            return Source.REMOVE;
        });
    }

    private List<BtAdapter> get_adapters() {
        List<BtAdapter> adapters = new List<BtAdapter>();
        obj_mgr.get_objects().foreach(obj => {
            DBusInterface? iface = obj.get_interface("org.bluez.Adapter1");
            if (iface == null) return;
            adapters.append(iface as BtAdapter);
        });
        return (owned) adapters;
    }

    private List<BtDevice> get_devices() {
        List<BtDevice> devices = new List<BtDevice>();
        obj_mgr.get_objects().foreach(obj => {
            DBusInterface? iface = obj.get_interface("org.bluez.Device1");
            if (iface == null) return;
            devices.append(iface as BtDevice);
        });
        return (owned) devices;
    }

    private void _update() {
        this.freeze_notify();

        // Enumerate all of the devices and adapters
        List<BtAdapter> adapters = get_adapters();
        List<BtDevice> devices = get_devices();

        // Check to see if we have bluetooth available
        available = (adapters.length() > 0);

        // Update general status of bluetooth
        powered = false;
        foreach (var adpt in adapters)
            if (adpt.powered) {
                powered = true;
                break;
            }

        connected = false;
        connection_name = null;
        foreach (var dev in devices)
            if (dev.connected) {
                connected = true;
                if (connection_name == null)
                    connection_name = dev.name;
                else
                    connection_name = "Multiple";
                break;
            }

        this.thaw_notify();
    }

    public async void set_bt_powered(bool state) {
        if (state == powered)
            return; // Nothing to do

        // Disconnect all the devices if needed
        if (state == false) {
            foreach (var dev in get_devices()) {
                if (dev.connected)
                    try {
                        yield dev.disconnect();
                    } catch (Error e) {
                        critical(e.message);
                    }
            }
        }

        // Power off the adapters
        foreach (var adpt in get_adapters())
            adpt.powered = state;

        // Queue update for our state when idle
        update();
    }

}
