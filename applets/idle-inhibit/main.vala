[DBus (name = "org.gnome.SessionManager")]
interface SessionIface : DBusProxy {
    public abstract void inhibit(string app_id, uint toplevel_xid,
                                 string reason, uint flags,
                                 out uint inhibit_cookie) throws Error;

    public abstract void uninhibit(uint cookie) throws Error;
}

private class InhibitorToggle : Panel.CCToggle {
    private SessionIface proxy;
    private uint? cookie = null;

    public InhibitorToggle(Panel.Signals sig) {
        Object(); // Init superclass

        this.panel_ordering = 50;
        this.name = "Stay\nAwake";
        this.status = null;
        this.multiline = true;
        this.icon_name = "caffine-off-symbolic";
        this.panel_tooltip = "Staying Awake";

        proxy = Bus.get_proxy_sync(BusType.SESSION,
                    "org.gnome.SessionManager", "/org/gnome/SessionManager");

        this.notify["active"].connect(() => toggled());
        sig.parsed["toggle-idle"].connect(() => {
            this.active = !this.active;
        });
    }

    private void toggled() {
        // Ask the session manager to inhibit
        try {
            if (this.active) {
                proxy.inhibit("carbonOS", 0, "Stay Awake Enabled",
                    8 /*idle*/, out cookie);
            } else {
                proxy.uninhibit(cookie);
            }
        } catch (Error e) {
            critical(e.message);
        }

        this.icon_name = this.active ? "caffine-on-symbolic" :
            "caffine-off-symbolic";
        this.panel_icon_name = this.active ? "caffine-on-symbolic" : null;
    }
}

public Panel.Applet? create_applet() {
    var sig = Panel.Signals.obtain();
    var toggle = new InhibitorToggle(sig);
    toggle.ref();
    sig.register_cc_toggle(toggle);
    return null;
}

