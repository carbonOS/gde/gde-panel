class OverviewApplet : Panel.Applet {
	construct {
		applet_name = "overview";

		var btn = new Gtk.Button.from_icon_name("multitasking-symbolic");
		btn.get_style_context().add_class(Gtk.STYLE_CLASS_FLAT);
		btn.tooltip_text = "Overview";
		btn.clicked.connect(() => {
            open_overview.begin();
		});
		panel_item = btn;
	}

	async void open_overview() {
        var conn = yield Bus.@get(BusType.SESSION);
        yield conn.call("org.wayland.compositor",
                        "/org/wayland/compositor",
                        "org.wayland.compositor",
                        "scale", new Variant("(bs)", false, "") /* all workspaces */,
                        null, DBusCallFlags.NONE, -1);
	}
}

public Panel.Applet? create_applet(bool in_lock) {
    if (in_lock)
        return null;
    else
	    return new OverviewApplet();
}
