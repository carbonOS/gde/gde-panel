class IconBox : Gtk.Box {
    public List<string> apps;
    public AppListApplet applet;
    public Gtk.Separator sep;

    public IconBox(AppListApplet appl) {
        orientation = Gtk.Orientation.HORIZONTAL;
        applet = appl;
        sep = new Gtk.Separator(Gtk.Orientation.VERTICAL);
    }

    public void load(string[] ids) {
        Wf.View[] handles = {};
        foreach (var child in get_children())
            if (child is Gtk.Separator)
                this.remove(child);
            else {
                foreach (var handle in (child as AppLauncher).handles)
                    handles += handle;
                child.destroy();
            }

        apps = new List<string>();
        foreach (var id in ids) {
            var launcher = new AppLauncher(applet, id, true, false);
            this.add(launcher);
            apps.append(id);
        }

        sep.get_style_context().add_class("hidden");
        this.add(sep);

        foreach (var handle in handles)
            add_handle(handle, false);

        this.show_all();
    }

    public bool contains(string id) {
        foreach (var app in apps)
            if (app == id) return true;
        return false;
    }

    public void add_handle(Wf.View handle, bool anim) {
        if (handle.app_id in this) {
            foreach (var child in get_children()) {
                var launcher = child as AppLauncher;
                if (launcher == null) continue;
                if (launcher.app.get_id() == handle.app_id + ".desktop") {
                    launcher.connect_handle(handle);
                    return;
                }
            }
        }
        // If no launcher was found, we should make it

        var launcher = new AppLauncher(applet, handle.app_id, false, anim);
        launcher.connect_handle(handle);
        apps.append(handle.app_id);
        launcher.destroy.connect(() => {
            // See https://valadoc.org/glib-2.0/GLib.List.remove_link.html
            unowned List<string> link =
                apps.find_custom(handle.app_id, strcmp);
            apps.remove_link(link);

            if (launcher == dragging) {
                set_drag_widget(null);
                sep.get_style_context().remove_class("dragging");
            }

            if (get_n_unpinned() > 0)
                sep.get_style_context().remove_class("hidden");
            else
                sep.get_style_context().add_class("hidden");
        });
        this.add(launcher);
        launcher.show_all();

        sep.get_style_context().remove_class("hidden");
    }

    private int get_n_unpinned() {
        var unpinned_apps = -1;
        foreach (var child in get_children())
            if (unpinned_apps >= 0 || child is Gtk.Separator)
                if (child != dragging || !(child as AppLauncher).pinned)
                    unpinned_apps++;
        return unpinned_apps;
    }

    public void activate_nth(int n, bool force_launch = false) {
        var children = get_children();
        var child = children.nth_data(n);
        if (child is Gtk.Separator)
            child = children.nth_data(n + 1);
        (child as AppLauncher).activate(force_launch);
    }

    // Drag and drop

    private Gtk.Widget dragging;
    private int drag_offset;
    private int drag_x;

    public void set_drag_widget(Gtk.Widget? widget) {
        this.dragging = widget;
        if (widget == null) {
            drag_x = -1;
            return;
        }

        Gtk.Allocation alloc;
        widget.get_allocation(out alloc);
        drag_x = alloc.x;
    }

    public void set_drag_offset(double offset) {
        drag_x += (int) offset;
        queue_allocate();
        queue_draw();
        this.reorder_child(dragging, get_index(dragging));
    }

    public override void size_allocate(Gtk.Allocation alloc) {
        base.size_allocate(alloc);
        if (dragging != null) {
            Gtk.Allocation drag_alloc;
            dragging.get_allocation(out drag_alloc);

            var max = alloc.x + alloc.width - drag_alloc.width;
            //if (get_n_unpinned() == 0)
            //    max += 5;
            if (drag_x > max)
                drag_x = max;
            if (drag_x < alloc.x)
                drag_x = alloc.x;

            drag_alloc.x = drag_x;
            dragging.size_allocate(drag_alloc);
        }
    }

    public int get_index(Gtk.Widget widget) {
        Gtk.Allocation w_alloc;
        widget.get_allocation(out w_alloc);

        int i = 0;
        foreach (var child in base.get_children()) {
            Gtk.Allocation alloc;
            child.get_allocation(out alloc);


            //if ((alloc.x + alloc.width / 2) <= (w_alloc.x + w_alloc.width)) {
            //    i++;
            //    break;
            //} else

            if ((alloc.x + alloc.width / 2) >= w_alloc.x)
                break;

            i++;
        }
        return i;
    }

    // Makes sure the widget being dragged is always on top
    public override void forall_internal(bool inc, Gtk.Callback callback) {
        var all = new List<Gtk.Widget>();
        base.forall_internal(inc, w => all.append(w));

        if (dragging != null) {
            all.remove(dragging);
            all.append(dragging);
        }

        foreach (var child in all)
            callback(child);
    }

    public void update_order() {
        var settings = new Settings("sh.carbon.gde-panel");
        string[] pinned_apps = {};
        foreach (var child in get_children()) {
            if (child is Gtk.Separator)
                break;
            var icon = child as AppLauncher;
            pinned_apps += icon.app.get_id().replace(".desktop", "");
        }
        settings.set_strv("pinned-apps", pinned_apps);
    }
}
