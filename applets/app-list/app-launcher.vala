 class AppLauncher : Gtk.Overlay {
    public DesktopAppInfo app;
    public List<Wf.View> handles;
    public Wf.View last_focused { get; set; default = null; }
    public bool pinned = false;

    private Icon app_icon; // Stored here for DND
    private Gtk.Image icon_view;
    private Gtk.Box indicator;
    private Gtk.Popover popover;
    private Gtk.Box menu_window_list;
    private Gtk.Button menu_close_all_btn;
    private Settings panel_settings;

    public AppLauncher(AppListApplet applet, string id, bool pinned, bool anim) {
        var info = new DesktopAppInfo(id + ".desktop");
        if (info == null) { // Check that info exists
            warning("Attempted to load invalid app: %s\n", id);
            app = new DesktopAppInfo("sh.carbon.Unknown.desktop");
        } else app = info;

        handles = new List<Wf.View>();
        this.pinned = pinned;
        this.size_allocate.connect(on_alloc_changed);

        var btn = new Gtk.Button();
        btn.tooltip_text = app.get_display_name();
        btn.get_style_context().add_class(Gtk.STYLE_CLASS_FLAT);
        btn.get_style_context().add_class("image-button");
        this.add(btn);

        app_icon = app.get_icon() ?? new ThemedIcon("application-x-executable");
        icon_view = new Gtk.Image.from_gicon(app_icon, Gtk.IconSize.LARGE_TOOLBAR);
        icon_view.pixel_size = 1;
        icon_view.get_style_context().add_class("icon-dropshadow");
        btn.add(icon_view);

        indicator = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 0);
        indicator.get_style_context().add_class("cSH-open-indicator");
        indicator.height_request = 2;
        panel_settings = new Settings("sh.carbon.gde-panel");
        indicator.halign = Gtk.Align.CENTER;
        indicator.valign = panel_settings.get_boolean("place-top") ?
            Gtk.Align.START : Gtk.Align.END;
        panel_settings.changed["place-top"].connect(() => {
            indicator.valign = panel_settings.get_boolean("place-top") ?
                Gtk.Align.START : Gtk.Align.END;
            this.remove(indicator);
            this.add_overlay(indicator);
        });
        this.add_overlay(indicator);

        // Menus
        popover = new Gtk.Popover(btn);
        var toggle_name = "app-list:%s".printf(app.get_id());
        Panel.setup_popover(applet, popover, false, toggle_name);
        var menu_contents = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
        menu_contents.width_request = 200;
        menu_contents.margin = 8;
        popover.add(menu_contents);

        menu_window_list = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
        menu_window_list.no_show_all = true;
        menu_contents.add(menu_window_list);
        var menu_contents_sep = new Gtk.Separator(Gtk.Orientation.HORIZONTAL);
        menu_contents_sep.visible = true;
        menu_window_list.pack_end(menu_contents_sep);

        var actions = app.list_actions();
        if (actions.length > 0) {
            var menu_actions = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
            foreach (var action in actions) {
                var action_button = new Gtk.Button();
                action_button.get_style_context().add_class("flat");
                var action_label = new Gtk.Label(app.get_action_name(action));
                action_label.halign = Gtk.Align.START;
                action_label.max_width_chars = 20;
                action_label.ellipsize = Pango.EllipsizeMode.END;
                action_button.add(action_label);
                action_button.clicked.connect(() => {
                    Carbon.Utils.launch(app, action);
                    popover.popdown();
                });
                menu_actions.add(action_button);
            }
            menu_actions.add(new Gtk.Separator(Gtk.Orientation.HORIZONTAL));
            menu_contents.add(menu_actions);
        }

        var menu_standard = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
        var menu_pin_btn = new Gtk.Button();
        menu_pin_btn.get_style_context().add_class("flat");
        var menu_pin_lbl = new Gtk.Label(pinned ? "Unpin" : "Pin");
        menu_pin_lbl.halign = Gtk.Align.START;
        menu_pin_btn.add(menu_pin_lbl);
        menu_pin_btn.clicked.connect(() => {
            var settings = new Settings("sh.carbon.gde-panel");
            var pinned_apps = settings.get_strv("pinned-apps");
            if (!pinned) {
                pinned_apps += app.get_id().replace(".desktop", "");
                settings.set_strv("pinned-apps", pinned_apps);
                this.destroy(); // We don't want to keep the icon in unpinned-box
            } else {
                string[] new_value = {};
                foreach (var pinned_id in pinned_apps)
                    if ((pinned_id + ".desktop") != app.get_id())
                        new_value += pinned_id;
                settings.set_strv("pinned-apps", new_value);
            }
        });
        menu_pin_btn.visible = app.get_id() != "sh.carbon.Unknown.desktop";
        menu_standard.add(menu_pin_btn);
        var menu_launch_btn = new Gtk.Button();
        menu_launch_btn.get_style_context().add_class("flat");
        var menu_launch_lbl = new Gtk.Label("Launch");
        menu_launch_lbl.halign = Gtk.Align.START;
        menu_launch_btn.add(menu_launch_lbl);
        menu_launch_btn.clicked.connect(() => activate(true));
        menu_standard.add(menu_launch_btn);
        menu_close_all_btn = new Gtk.Button();
        menu_close_all_btn.get_style_context().add_class("flat");
        menu_close_all_btn.no_show_all = true;
        var menu_close_all_lbl = new Gtk.Label("Quit");
        menu_close_all_lbl.halign = Gtk.Align.START;
        menu_close_all_lbl.visible = true;
        menu_close_all_btn.add(menu_close_all_lbl);
        menu_close_all_btn.clicked.connect(() => {
            close_all();
            popover.popdown();
        });
        menu_close_all_btn.show_all();
        menu_standard.pack_end(menu_close_all_btn);
        menu_contents.add(menu_standard);

        menu_contents.show_all();

        // Button input handling
        var ignore_click = false;
        btn.button_release_event.connect(evt => {
            if (ignore_click) {
                ignore_click = false;
                return Gdk.EVENT_PROPAGATE;
            }
            if (evt.button == Gdk.BUTTON_PRIMARY)
                activate();
            else if (evt.button == Gdk.BUTTON_SECONDARY)
                if (popover.visible)
                    popover.popdown();
                else
                    popover.popup();
            else if (evt.button == Gdk.BUTTON_MIDDLE)
                activate(true);
            return Gdk.EVENT_PROPAGATE;
        });
        var long_press = new Gtk.GestureLongPress(btn);
        long_press.pressed.connect((x, y) => {
            ignore_click = true;
            if (popover.visible)
                popover.popdown();
            else
                popover.popup();
        });
        long_press.touch_only = true;
        btn.set_data("long_press", long_press);

        // Drag & drop
        var drag = new Gtk.GestureDrag(btn);
        IconBox container = null;
        var initial_index = 0;
        var locked = true;
        drag.drag_begin.connect((start_x, start_y) => {
            container = this.get_parent() as IconBox;
            container.set_drag_widget(this);
            initial_index = container.get_index(this);
        });
        drag.drag_update.connect((x, y) => {
            if (x.abs() > 3)
                locked = false;
            if (locked) return;
            popover.popdown(); // Make sure the menu isn't open
            container.set_drag_offset(x);
            container.sep.get_style_context().add_class("dragging");
            ignore_click = true; // This is not just a button press
        });
        drag.drag_end.connect(() => {
            container.set_drag_widget(null);
            if (container.get_index(this) != initial_index)
                container.update_order();
            container.sep.get_style_context().remove_class("dragging");
            locked = true;
        });
        btn.set_data("drag", drag);

        // Show the icon
        if (!pinned && anim)
            animate_show();
        else
            icon_view.pixel_size = 30;
    }

    public void activate(bool force_launch = false) {
        if (!force_launch && last_focused != null)
            last_focused.activate();
        else
            Carbon.Utils.launch(app);
        popover.popdown();
    }

    public void close_all() {
        foreach (var handle in handles)
            handle.close();
    }

    private void update_status() {
        var cxt = indicator.get_style_context();
        if (handles.length() > 0) {
            cxt.add_class("active");
            menu_window_list.visible = true;
            menu_close_all_btn.visible = true;
        } else {
            cxt.remove_class("active");
            menu_window_list.visible = false;
            menu_close_all_btn.visible = false;
        }

        var focused = false;
        foreach (var handle in handles)
            if (handle.focused) {
                focused = true;
                last_focused = handle;
            }
        if (focused) {
            cxt.add_class("focused");
            cxt.remove_class("active");
        } else {
            cxt.remove_class("focused");
        }
    }

    public bool contains(Wf.View cmp) {
        foreach (var handle in handles)
            if (handle == cmp) return true;
        return false;
    }

    public void connect_handle(Wf.View handle) {
        if (handle in this)
            return;
        handle.notify.connect(update_status);
        handle.destroy.connect(() => {
            handles.remove(handle);
            update_status();
            if (!pinned && handles.length() == 0)
                animate_then_destroy();
            last_focused = handles.nth_data(0);
        });
        handles.append(handle);
        if (last_focused == null) last_focused = handle;
        update_status();

        // Add an entry to the menu's window list
        var handle_entry = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 0);

        var handle_btn = new Gtk.Button();
        handle_btn.get_style_context().add_class("flat");
        var handle_lbl = new Gtk.Label(null);
        handle.bind_property("title", handle_lbl, "label",
            BindingFlags.SYNC_CREATE);
        handle_lbl.halign = Gtk.Align.START;
        handle_lbl.max_width_chars = 20;
        handle_lbl.ellipsize = Pango.EllipsizeMode.END;
        handle_btn.add(handle_lbl);
        handle_btn.clicked.connect(() => {
            handle.activate();
            popover.popdown();
        });
        handle_entry.pack_start(handle_btn);

        var close_btn = new Gtk.Button();
        var close_btn_icon = new Gtk.Image.from_icon_name("window-close-symbolic",
            Gtk.IconSize.MENU);
        close_btn.add(close_btn_icon);
        close_btn.valign = Gtk.Align.CENTER;
        close_btn.get_style_context().add_class("titlebutton");
        close_btn.clicked.connect(() => {
            handle.close();
            popover.popdown();
        });
        handle_entry.pack_end(close_btn, false);

        /* TODO: Keep above doesn't work
        var pin_top_btn = new Gtk.ToggleButton();
        pin_top_btn.image = new Gtk.Image.from_icon_name("view-pin-symbolic",
            Gtk.IconSize.BUTTON);
        pin_top_btn.get_style_context().add_class("flat");
        handle.bind_property("keep-above", pin_top_btn, "active",
            BindingFlags.SYNC_CREATE | BindingFlags.BIDIRECTIONAL);
        handle_entry.pack_end(pin_top_btn, false);*/

        menu_window_list.add(handle_entry);
        handle_entry.show_all();
        handle.destroy.connect(() => handle_entry.destroy());
    }

    public void on_alloc_changed(Gtk.Allocation alloc) {
        Gdk.Rectangle rect = { alloc.x, alloc.y, alloc.width, alloc.height};
        foreach (var handle in handles)
            handle.set_minimize_rect(rect);
    }

    private void animate_show() {
        var anim = Object.new(typeof(Dazzle.Animation),
                              "target", this.icon_view,
                              "duration", 200,
                              null) as Dazzle.Animation;
        var clazz = (ObjectClass) typeof(Gtk.Image).class_ref();
        anim.add_property(clazz.find_property("pixel-size"), 30);
        anim.start();
        Idle.add(() => {
            if (icon_view.pixel_size < 30)
                return Source.CONTINUE;
            else {
                this.get_style_context().remove_class("small");
                return Source.REMOVE;
            }
        });
    }

    private void animate_then_destroy() {
        this.get_style_context().add_class("small"); // Hides margins & padding
        var anim = Object.new(typeof(Dazzle.Animation),
                              "target", this.icon_view,
                              "duration", 200,
                              null) as Dazzle.Animation;
        var clazz = (ObjectClass) typeof(Gtk.Image).class_ref();
        anim.add_property(clazz.find_property("pixel-size"), 1);
        anim.start();
        Idle.add(() => {
            if (icon_view.pixel_size > 1)
                return Source.CONTINUE;
            else {
                this.destroy();
                return Source.REMOVE;
            }
        });
    }
}
