class AppListApplet : Panel.Applet {
    private IconBox box;
    private Settings settings;
    private Wf.TaskManager taskman;

    construct {
        applet_name = "app-list";
        settings = new Settings("sh.carbon.gde-panel");
        panel_item = create_panel_item();

        // Reload when the pinned-apps setting changes
        settings.changed["pinned-apps"].connect(() => {
            box.load(settings.get_strv("pinned-apps"));
        });

        // Load in the apps
        taskman = new Wf.TaskManager();
        taskman.view_added.connect(view => {
            box.add_handle(view, /*animate*/ true);
        });
        taskman.setup.begin();

        // Handle the keyboard shortcuts
        Panel.Signals.obtain().parsed.connect((cmd, data) => {
            var force = cmd == "launch-pinned";
            if (force || cmd == "activate-pinned")
                box.activate_nth(int.parse(data), force);
        });
    }

    private Gtk.Widget create_panel_item() {
        box = new IconBox(this);
        box.load(settings.get_strv("pinned-apps"));
        box.get_style_context().add_class("cSH-icon-box");

        Gtk.drag_dest_set(box, Gtk.DestDefaults.ALL, {}, Gdk.DragAction.COPY);
        Gtk.drag_dest_add_uri_targets(box);
        box.drag_data_received.connect((ctx, x, y, data, info, time) => {
            var uri = data.get_uris()[0];
            var app = new DesktopAppInfo.from_filename(uri.replace("file://", ""));
            var apps = settings.get_strv("pinned-apps");
            var id = app.get_id().replace(".desktop", "");
            if (!(id in apps))
                apps += id;
            settings.set_strv("pinned-apps", apps);
        });

        return box;
    }
}

public Panel.Applet? create_applet(bool in_lock) {
    if (in_lock)
        return null;
    else
	    return new AppListApplet();
}
