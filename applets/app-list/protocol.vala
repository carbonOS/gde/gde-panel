[DBus(name = "org.wayland.compositor")]
interface Wf.Proxy : DBusProxy {
    [DBus(name = "view_added")]
    public abstract signal void view_added(uint32 id);
    [DBus(name = "view_closed")]
    public abstract signal void view_closed(uint32 id);
    [DBus(name = "view_title_changed")]
    public abstract signal void view_title_changed(uint32 id, string title);
    [DBus(name = "view_attention_changed")]
    public abstract signal void view_attention_changed(uint32 id, bool attention);
    [DBus(name = "view_maximized_changed")]
    public abstract signal void view_maximized_changed(uint32 id, bool maximized);
    [DBus(name = "view_minimized_changed")]
    public abstract signal void view_minimized_changed(uint32 id, bool minimized);
    [DBus(name = "view_keep_above_changed")]
    public abstract signal void view_keep_above_changed(uint32 id, bool above);
    [DBus(name = "view_focus_changed")]
    public abstract signal void view_focus_changed(uint32 id);

    [DBus(name = "query_view_vector_taskman_ids")]
    public abstract uint32[] query_view_vector_taskman_ids() throws Error;
    [DBus(name = "query_view_credentials")]
    public abstract void query_view_credentials(uint32 id, out int32 pid,
        out uint32 uid, out uint32 gid) throws Error;
    [DBus(name = "query_view_app_id")]
    public abstract string query_view_app_id(uint32 id) throws Error;
    [DBus(name = "query_view_app_id_gtk_shell")]
    public abstract string query_view_app_id_gtk_shell(uint32 id) throws Error;
    [DBus(name = "query_view_title")]
    public abstract string query_view_title(uint32 id) throws Error;
    [DBus(name = "query_view_attention")]
    public abstract bool query_view_attention(uint32 id) throws Error;
    [DBus(name = "query_view_maximized")]
    public abstract bool query_view_maximized(uint32 id) throws Error;
    [DBus(name = "query_view_minimized")]
    public abstract bool query_view_minimized(uint32 id) throws Error;
    [DBus(name = "query_view_above")]
    public abstract bool query_view_above(uint32 id) throws Error;
    [DBus(name = "query_view_active")]
    public abstract bool query_view_active(uint32 id) throws Error;
    [DBus(name = "query_view_group_leader")]
    public abstract uint32 query_view_group_leader(uint32 id) throws Error;
    [DBus(name = "query_view_role")]
    public abstract uint32 query_view_role(uint32 id) throws Error;

    [DBus(name = "minimize_view")]
    public abstract void minimize_view(uint32 id, uint32 action) throws Error;
    [DBus(name = "maximize_view")]
    public abstract void maximize_view(uint32 id, uint32 action) throws Error;
    [DBus(name = "focus_view")]
    public abstract void focus_view(uint32 id, uint32 action) throws Error;
    [DBus(name = "close_view")]
    public abstract void close_view(uint32 id) throws Error;
    [DBus(name = "change_view_minimize_hint")]
    public abstract void change_view_minimize_hint(uint32 id, int32 a, int32 b,
        int32 c, int32 d) throws Error;
    [DBus(name = "change_view_above")]
    public abstract void change_view_above(uint32 id, uint32 action) throws Error;

    [DBus(name = "bring_view_to_front")]
    public abstract void bring_view_to_front(uint32 id) throws Error; // ?
    [DBus(name = "update_view_minimize_hint")]
    public abstract void update_view_minimize_hint(uint32 id) throws Error; // ?
}

class Wf.View : Object {
    private Wf.Proxy proxy;
    internal uint32 id;

    public View(Wf.Proxy proxy, uint32 id) {
        this.proxy = proxy;
        this.id = id;

        try {
            // Find the app for this view
            int32 pid;
            uint32 uid, gid; // Unused
            proxy.query_view_credentials(id, out pid, out uid, out gid);
            var app_id = Panel.get_flatpak_appid_from_pid((uint32) pid);
            if (app_id == null) app_id = proxy.query_view_app_id_gtk_shell(id);
            if (app_id == "nullptr") app_id = proxy.query_view_app_id(id);
            if (app_id == "nullptr") {
                warning("Unknown app from dbus; view-id:%u, PID:%u", id, pid);
                app_id = "sh.carbon.Unknown";
            }
            this.app_id = Panel.fix_app_id(app_id);

            // Pre-load the properties
            this.title = proxy.query_view_title(id);
            this._maximized = proxy.query_view_maximized(id);
            this._minimized = proxy.query_view_minimized(id);
            this._keep_above = proxy.query_view_above(id);
            this._focused = proxy.query_view_active(id);
        } catch (Error e) {
            critical("Failed to set-up view %u: %s", id, e.message);
            this.destroy();
        }
    }

    public string app_id;
    public string title { get; internal set; }
    internal bool _maximized;
    public bool maximized {
        get { return _maximized; }
        set {
            _maximized = value;
            proxy.maximize_view(id, value ? 1 : 0);
        }
    }
    internal bool _minimized;
    public bool minimized {
        get { return _minimized; }
        set {
            _minimized = value;
            proxy.minimize_view(id, value ? 1 : 0);
        }
    }
    internal bool _keep_above;
    public bool keep_above {
        get { return _keep_above; }
        set {
            _keep_above = value;
            proxy.change_view_above(id, value ? 1 : 0);
        }
    }
    internal bool _focused;
    public bool focused {
        get { return _focused; }
        set {
            _focused = value;
            proxy.focus_view(id, value ? 1 : 0);
        }
    }

    public void activate() {
        if (minimized) {
            minimized = false;
            focused = true;
        } else if (focused) {
            minimized = true;
        } else {
            focused = true;
        }
    }

    public void close() {
        proxy.close_view(id);
    }

    public void set_minimize_rect(Gdk.Rectangle rect) {
        proxy.change_view_minimize_hint(id, rect.x, rect.y, rect.width, rect.height);
        proxy.update_view_minimize_hint(id);
    }

    public signal void destroy(); // Destroys this object, but not the view itself
}

class Wf.TaskManager {
    private Wf.Proxy proxy;
    private HashTable<uint32, Wf.View> views;

    public TaskManager() {
        views = new HashTable<uint32, Wf.View>(direct_hash, direct_equal);
    }

    public async void setup() {
        proxy = yield Bus.get_proxy<Wf.Proxy?>(BusType.SESSION,
            "org.wayland.compositor", "/org/wayland/compositor");

        // Load in the already-open views
        foreach (var id in proxy.query_view_vector_taskman_ids())
            on_new_view(id);

        // Connect the signals
        proxy.view_added.connect(on_new_view);
        proxy.view_closed.connect(id => {
            var view = views[id];
            if (view != null) view.destroy();
        });
        proxy.view_title_changed.connect((id, val) => {
            var view = views[id];
            if (view != null) view.title = val;
        });
        proxy.view_maximized_changed.connect((id, val) => {
            var view = views[id];
            if (view != null) {
                view._maximized = val;
                view.notify_property("maximized");
            }
        });
        proxy.view_minimized_changed.connect((id, val) => {
            var view = views[id];
            if (view != null) {
                view._minimized = val;
                view.notify_property("minimized");
            }
        });
        proxy.view_keep_above_changed.connect((id, val) => {
            var view = views[id];
            if (view != null) {
                view._keep_above = val;
                view.notify_property("keep-above");
            }
        });
        proxy.view_focus_changed.connect(id => {
            foreach (var view in views.get_values()) {
                view._focused = (view.id == id);
                view.notify_property("focused");
            }
        });
    }

    public signal void view_added(Wf.View view);

    private void on_new_view(uint32 id) {
        try {
            var parent_id = proxy.query_view_group_leader(id);
            var role = proxy.query_view_role(id);
            if (parent_id != id || role != 1) // Ignore w/ parents or non-toplevel
                return;
        } catch (Error e) {
            return; // Something went wrong. Bail
        }

        var view = new Wf.View(proxy, id);
        views[id] = view;
        view.destroy.connect(() => {
           views.remove(id);
        });
        view_added(view);
    }
}
