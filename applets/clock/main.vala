public Panel.Applet? create_applet(bool in_lock) {
    var info = new Panel.CCInfo();
    info.panel_ordering = 0;
    info.icon_name = null;
    info.label = null;

    // Set up the clock
    var wallclock = new Gnome.WallClock();
    wallclock.time_only = true;
    info.notify["panel-text"].connect(() => {
        // Update the calandar date
        var datetime = new DateTime.now(wallclock.get_timezone());
        info.panel_tooltip = datetime.format("%a, %b %-e");
    });
    wallclock.bind_property("clock", info, "panel-text", BindingFlags.SYNC_CREATE);

    // Register the info source
    Panel.Signals.obtain().register_cc_info(info, "");
	return null; // We don't want a regular applet
}
