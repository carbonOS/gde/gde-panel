// Everything from g-s-d-rfkill that we care about here
[DBus (name = "org.gnome.SettingsDaemon.Rfkill")]
public interface RfKill : DBusProxy {
	public abstract bool airplane_mode { public owned get; public set; }
	public abstract bool should_show_airplane_mode { public owned get;}
}

private void update_rfkill_toggle(Panel.CCToggle toggle, RfKill proxy) {
    toggle.icon_name = proxy.airplane_mode ?
        "airplane-mode-symbolic" : "airplane-mode-disabled-symbolic";
    toggle.panel_icon_name = proxy.airplane_mode ?
        "airplane-mode-symbolic" : null;
    toggle.panel_tooltip = proxy.airplane_mode ?
        "Airplane Mode" : null;
    toggle.active = proxy.airplane_mode;
    toggle.visible = proxy.should_show_airplane_mode;
}

async void setup_toggle(Panel.Signals sig) {
    // Get the proxy
    var proxy = yield Bus.get_proxy<RfKill?>(BusType.SESSION,
        "org.gnome.SettingsDaemon.Rfkill", "/org/gnome/SettingsDaemon/Rfkill");
    if (proxy == null) return;

    // Create the toggle
    var toggle = new Panel.CCToggle();
    toggle.ordering = 25;
    toggle.panel_ordering = 32;
    toggle.name = "Airplane\nMode";
    toggle.active = proxy.airplane_mode;
    toggle.status = null;
    toggle.multiline = true;
    toggle.more_settings = "gnome-wifi-panel.desktop";

    // Bind the toggle to the proxy
    toggle.notify["active"].connect(() => {
        proxy.airplane_mode = toggle.active;
    });
    proxy.g_properties_changed.connect((a, b) => {
      update_rfkill_toggle(toggle, proxy);
    });
    update_rfkill_toggle(toggle, proxy);

    // Register w/ the panel/
    sig.register_cc_toggle(toggle);
}

public Panel.Applet? create_applet(bool in_lock) {
    var sig = Panel.Signals.obtain();
    setup_toggle.begin(sig);
    return null;
}
