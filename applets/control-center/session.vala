async void prompt_power_off() {
    var conn = yield Bus.@get(BusType.SESSION);
    yield conn.call("sh.carbon.gde.EndSessionDialog", "/",
                    "org.gnome.SessionManager.EndSessionDialog",
                    "PromptForAction", null, null,
                    DBusCallFlags.NONE, -1);
}

async void lock_session() {
    var conn = yield Bus.@get(BusType.SYSTEM);
    yield conn.call("org.freedesktop.login1",
                    "/org/freedesktop/login1",
                    "org.freedesktop.login1.Manager",
                    "Suspend", new Variant("(b)", true) /* interactive */,
                    null, DBusCallFlags.NONE, -1);
}

void _call_gsm(AsyncResult res, string method, Variant? param) {
    DBusConnection conn;
    try {
        conn = Bus.@get.end(res);
    } catch (IOError e) {
        critical("Couldn't obtain session bus: %s", e.message);
        return;
    }
    conn.call.begin("org.gnome.SessionManager", "/org/gnome/SessionManager",
        "org.gnome.SessionManager", method, param,
        VariantType.UNIT, DBusCallFlags.NONE, -1, null, (o, r) => {
            try {
                conn.call.end(r);
            } catch (Error e) {
                critical("Coudldn't ask GSM to logout: %s", e.message);
            }
        });
}

Gtk.Button create_button(string icon, Callback callback) {
    Gtk.Button button = new Gtk.Button.from_icon_name(icon);
    button.get_style_context().add_class("circular");
    button.show_all();
    button.clicked.connect(() => {
        callback();
        Panel.Signals.obtain().on_hide_popovers("!!!");
    });
    button.valign = Gtk.Align.CENTER;
    return button;
}

[DBus (name = "org.freedesktop.Accounts.User")]
public interface ActUser : DBusProxy {
	public abstract string icon_file { public owned get; }
}

void add_avatar(Gtk.Box layout) {
    var name = Environment.get_real_name();
    var uid = (int) Posix.getuid();
    var avatar = new Hdy.Avatar(36, name, false);

    var proxy = Bus.get_proxy_sync<ActUser>(BusType.SYSTEM,
        "org.freedesktop.Accounts",
        "/org/freedesktop/Accounts/User" + uid.to_string());

    //avatar.set_loadable_icon(new Gdk.Pixbuf.from_file(proxy.icon_file));
    avatar.set_image_load_func(() => new Gdk.Pixbuf.from_file(proxy.icon_file));
    proxy.g_properties_changed.connect(() => {
        avatar.set_image_load_func(() => new Gdk.Pixbuf.from_file(proxy.icon_file));
    });

    var button = new Gtk.Button();
    button.get_style_context().add_class("circular");
    button.get_style_context().add_class("nopadding");
    button.clicked.connect(() =>
        Carbon.Utils.launch(new DesktopAppInfo("gnome-user-accounts-panel.desktop")));
    button.tooltip_text = name;
    button.add(avatar);
    layout.pack_start(button, false);
}

void add_session_buttons(Gtk.Box layout) {
    var poweroff = create_button("system-shutdown-symbolic", () => {
        prompt_power_off.begin();
    });
    poweroff.tooltip_text = "Power Off";
    layout.pack_end(poweroff, false, false);

    var suspend = create_button("system-lock-screen-symbolic", () => {
        lock_session.begin();
    });
    suspend.tooltip_text = "Lock";
    layout.pack_end(suspend, false);

    var settings = create_button("org.gnome.Settings-symbolic", () => {
        var app = new DesktopAppInfo("org.gnome.Settings.desktop");
        Carbon.Utils.launch(app, null);
    });
    settings.tooltip_text = "Settings";
    layout.pack_end(settings, false, false);
}
