class ControlCenterApplet : Panel.Applet {
	construct {
		applet_name = "control-center";
		var sig = Panel.Signals.obtain();

        var btn = Panel.create_button(null);
        var btn_container = new Gtk.FlowBox();
        btn_container.selection_mode = Gtk.SelectionMode.NONE;
		btn_container.orientation = Gtk.Orientation.VERTICAL;
		btn_container.homogeneous = false;
		btn_container.column_spacing = 6;
		btn_container.set_sort_func((a, b) => {
		    int first = a.get_data<int>("ordering");
		    int second = b.get_data<int>("ordering");
		    return second - first;
		});
        btn.add(btn_container);
        this.panel_item = btn;

        var popover = Panel.create_popover(this, btn);
        popover.width_request = 350;
        var layout = new Gtk.Box(Gtk.Orientation.VERTICAL, 12);
        layout.margin = 12;

        var info_box = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 8);
        layout.add(info_box);
        sig.register_cc_info.connect((info, action) => {
            var widget = create_info_button(info, action);
            widget.valign = Gtk.Align.CENTER;
            info_box.pack_start(widget, false);

            btn_container.add(create_panel_item(info));
        });
        add_avatar(info_box);
        add_session_buttons(info_box);

        var toggle_flow = new Gtk.FlowBox();
        toggle_flow.selection_mode = Gtk.SelectionMode.NONE;
        toggle_flow.orientation = Gtk.Orientation.HORIZONTAL;
        toggle_flow.homogeneous = false;
        toggle_flow.row_spacing = toggle_flow.column_spacing = 12;
        toggle_flow.min_children_per_line = 3;
        toggle_flow.max_children_per_line = 3;
		toggle_flow.set_sort_func((a, b) => {
		    int first = a.get_data<int>("ordering");
		    int second = b.get_data<int>("ordering");
		    return first - second;
		});
		layout.add(toggle_flow);
        sig.register_cc_toggle.connect(toggle => {
            toggle_flow.add(create_toggle_item(toggle));
            btn_container.add(create_panel_item(toggle));
        });

        var custom_box = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
        layout.add(custom_box);
        sig.register_cc_custom.connect(custom => {
            custom.widget.show_all();
            custom_box.pack_end(custom.widget);

            btn_container.add(create_panel_item(custom));
        });

        popover.add(layout);
        layout.show_all();

        // Don't show the popover if we're in the greeter
        if (Environment.get_variable("XDG_SESSION_CLASS") == "greeter")
            btn.sensitive = false;
	}

	private Gtk.Widget create_panel_item(Panel.CCApplet obj) {
	    var item = create_img_label(obj, "panel-icon-name", "panel-text",
	        "panel-tooltip");

	    // Wrap in a sortable FlowBoxChild
	    var child = new Gtk.FlowBoxChild();
	    child.no_show_all = true;
	    child.add(item);
	    child.set_data<int>("ordering", obj.panel_ordering);
	    item.bind_property("visible", child, "visible", BindingFlags.SYNC_CREATE);
	    return child;
	}

	private Gtk.Widget create_info_button(Panel.CCInfo obj, string action) {
	    var btn = new Gtk.Button();
	    btn.get_style_context().add_class("circular");
	    btn.get_style_context().add_class("ccinfo");

	    var content = create_img_label(obj, "icon-name", "label");
	    content.margin_start = content.margin_end = 6;
	    btn.add(content);

	    btn.clicked.connect(() =>
	        Carbon.Utils.launch(new DesktopAppInfo(action)));

	    btn.show_all();
	    content.bind_property("visible", btn, "visible", BindingFlags.SYNC_CREATE);
	    return btn;
	}

    private Gtk.Widget create_img_label(Panel.CCApplet obj, string img_prop,
                string label_prop, string? tooltip_prop = null) {
        var wrap = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 2);
        wrap.no_show_all = true;
        var img = new Gtk.Image();
        img.pixel_size = 16;
        img.no_show_all = true;
        wrap.add(img);
        var lbl = new Gtk.Label(null);
        lbl.use_markup = true;
        lbl.no_show_all = true;
        wrap.add(lbl);
        wrap.show_all();

        img.notify["visible"].connect(() => {
            wrap.visible = (img.visible || lbl.visible);
        });
        lbl.notify["visible"].connect(() => {
            wrap.visible = (img.visible || lbl.visible);
        });

        bind_with_visibility(obj, img_prop, img, "icon-name");
        bind_with_visibility(obj, label_prop, lbl, "label");

        if (tooltip_prop != null)
            obj.bind_property(tooltip_prop, wrap, "tooltip-text",
                BindingFlags.SYNC_CREATE | BindingFlags.BIDIRECTIONAL);

        return wrap;
    }

    private void bind_with_visibility(Panel.CCApplet obj, string src_prop,
            Gtk.Widget dest, string dest_prop) {
        obj.bind_property(src_prop, dest, dest_prop, BindingFlags.SYNC_CREATE);
        obj.bind_property(src_prop, dest, "visible", BindingFlags.SYNC_CREATE,
            (binding, from, ref to) => {
                to = (from.get_string() != null);
                return true;
            });
    }

    private Gtk.Widget create_toggle_item(Panel.CCToggle toggle) {
        var root = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);

        var btn = new Gtk.ToggleButton();
        btn.halign = Gtk.Align.CENTER;
        btn.valign = Gtk.Align.CENTER;
        btn.get_style_context().add_class("image-button");
        btn.get_style_context().add_class("circular");
        btn.get_style_context().add_class("cctoggle");
        btn.margin_bottom = 6;
        root.pack_start(btn);
        var img = new Gtk.Image();
        img.margin = 12;
        img.pixel_size = 24;
        toggle.bind_property("icon-name", img, "icon-name",
            BindingFlags.SYNC_CREATE);
        toggle.bind_property("active", btn, "active",
            BindingFlags.SYNC_CREATE | BindingFlags.BIDIRECTIONAL);
        btn.add(img);

        var lbl = new Gtk.Label(null);
        lbl.halign = Gtk.Align.CENTER;
        lbl.justify = Gtk.Justification.CENTER;
        toggle.bind_property("name", lbl, "label", BindingFlags.SYNC_CREATE);
        toggle.bind_property("max-width-chars", lbl, "max-width-chars",
            BindingFlags.SYNC_CREATE);
        toggle.bind_property("max-width-chars", lbl, "width-chars",
            BindingFlags.SYNC_CREATE);
        root.pack_start(lbl, false);

        if (!toggle.multiline) {
            var status = new Gtk.Label(null);
            status.use_markup = true;
            status.wrap_mode = Pango.WrapMode.CHAR;
            status.ellipsize = Pango.EllipsizeMode.END;
            status.halign = Gtk.Align.CENTER;
            status.get_style_context().add_class("dim-label");
            toggle.bind_property("status", status, "label", BindingFlags.SYNC_CREATE);
            toggle.bind_property("max-width-chars", status, "max-width-chars",
                BindingFlags.SYNC_CREATE);
            toggle.bind_property("max-width-chars", status, "width-chars",
                BindingFlags.SYNC_CREATE);
            root.pack_start(status, false);
        }

        // More settings option handling
        var ignore_click = false;
        btn.button_release_event.connect(evt => {
            if (ignore_click) {
                ignore_click = false;
                return Gdk.EVENT_STOP;
            }
            if (evt.button == Gdk.BUTTON_SECONDARY)
                launch_more_settings(toggle);
            return Gdk.EVENT_PROPAGATE;
        });
        var long_press = new Gtk.GestureLongPress(btn);
        long_press.pressed.connect((x, y) => {
            ignore_click = true;
            launch_more_settings(toggle);
        });
        long_press.touch_only = true;
        btn.set_data("long_press", long_press); // Keep a reference around

        Gtk.FlowBoxChild flow_child = new Gtk.FlowBoxChild();
        flow_child.add(root);
        flow_child.show_all();
        toggle.bind_property("visible", flow_child, "visible",
            BindingFlags.SYNC_CREATE);
        flow_child.set_data<int>("ordering", toggle.ordering);
        return flow_child;
    }

    void launch_more_settings(Panel.CCToggle toggle) {
        if (toggle.more_settings == null)
            return;
        var app = new DesktopAppInfo(toggle.more_settings);
        if (app == null)
            return;
        Carbon.Utils.launch(app, null);
    }
}

public Panel.Applet? create_applet() {
	return new ControlCenterApplet();
}
