[DBus (name = "org.gnome.SettingsDaemon.Power.Screen")]
interface GsdPower : DBusProxy {
    public abstract int32 brightness { get; set; }
    public abstract void step_up(out int32 new_value, out string connector);
    public abstract void step_down(out int32 new_value, out string connector);
}

async void create_brightness_slider(Panel.Signals sig) {
    // Create the connection to g-s-d-power
    var proxy = yield Bus.get_proxy<GsdPower?>(BusType.SESSION,
        "org.gnome.SettingsDaemon.Power", "/org/gnome/SettingsDaemon/Power");
    if (proxy == null) return;

    // Connect the brightness-up and brightness-down signals
    // TODO: Have gsd-media-keys handle this instead
    bool handling_kbd = false;
    sig.parsed["brightness-up"].connect(() => {
        int32 new_value; string connector; // Unused but required variables
        handling_kbd = true;
        proxy.step_up(out new_value, out connector);
    });
    sig.parsed["brightness-down"].connect(() => {
        int32 new_value; string connector; // Unused but required variables
        handling_kbd = true;
        proxy.step_down(out new_value, out connector);
    });

    // Create the UI
    var root = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 8);
    var icon = new Gtk.Image.from_icon_name("display-brightness-symbolic",
        Gtk.IconSize.BUTTON);
    icon.tooltip_text = "Brightness";
    root.pack_start(icon, false);
    var scale = new Gtk.Scale.with_range(Gtk.Orientation.HORIZONTAL, 0, 100, 1);
    scale.digits = 0;
    scale.draw_value = false;
    root.pack_start(scale);

    // Bind the slider to the proxy
    scale.set_value(proxy.brightness);
    ulong scale_cb = scale.value_changed.connect(() => {
        proxy.brightness = (int) scale.get_value();
    });
    proxy.g_properties_changed.connect(() => {
        root.visible = proxy.brightness >= 0;
        SignalHandler.block(scale, scale_cb);
        if (root.visible) scale.set_value(proxy.brightness);
        SignalHandler.unblock(scale, scale_cb);

        if (handling_kbd) {
            Panel.show_shell_osd.begin("display-brightness-symbolic", null,
                proxy.brightness / 100.0);
            handling_kbd = false;
        }
    });

    // Register w/ the panel
    sig.register_cc_custom(new Panel.CCCustom(root));
}
