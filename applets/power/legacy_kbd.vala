// TODO: Drop this
// carbonSHELL will no longer be handling keyboard brightness to better align
// with the rest of the OS ecosystem. g-s-d-media-keys should now handle keyboard
// brightness itself via the keyboard keys. However, since g-s-d-media-keys
// doesn't function yet, we're keeping this code around for now

[DBus (name = "org.freedesktop.UPower.KbdBacklight")]
public interface KbdBacklight : DBusProxy {
    public abstract int32 get_brightness();
    public abstract int32 get_max_brightness();
    public abstract void set_brightness(int32 to);
    public signal void brightness_changed(int32 to);
    public signal void brightness_changed_with_source(int32 to, string source);
}

private void setup_legacy_kbd_signals(Panel.Signals sig) {
    Bus.get_proxy.begin<KbdBacklight?>(BusType.SYSTEM,
        "org.freedesktop.UPower", "/org/freedesktop/UPower/KbdBacklight",
        DBusProxyFlags.NONE, null, (o, r) => {
            try {
                KbdBacklight? backlight = Bus.get_proxy.end(r);
                if (backlight == null) return;
                sig.parsed["kbd-brightness-up"].connect(() => {
                    backlight.set_brightness(backlight.get_brightness() + 5);
                    var percent = backlight.get_brightness() /
                        (double) backlight.get_max_brightness();
                    Panel.show_shell_osd.begin("keyboard-brightness-symbolic",
                        null, percent);
                });
                sig.parsed["kbd-brightness-down"].connect(() => {
                    backlight.set_brightness(backlight.get_brightness() - 5);
                    var percent = backlight.get_brightness() /
                        (double) backlight.get_max_brightness();
                    Panel.show_shell_osd.begin("keyboard-brightness-symbolic",
                        null, percent);
                });
            } catch (Error e) {
                info("No keyboard backlight");
                return;
            }
        });
}
