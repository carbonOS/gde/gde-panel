using UPower;

private void update_bat_info(Panel.CCInfo info, Device device, Settings settings) {
    if (!device.is_present) { // Hide everything
        info.icon_name = null;
        info.label = null;
        info.panel_text = null;
        return; // Nothing else to do here
    } else if (device.Type == DeviceType.BATTERY) {
        // Show the battery percentage
        info.label = "%0.lf%%".printf(device.percentage);

        // Try to use the fine battery icons
        var level = 10 * Math.floor(device.percentage / 10);
        var charging = (device.state == DeviceState.CHARGING) ? "-charging" : "";
        if (device.state == DeviceState.FULLY_CHARGED)
            info.icon_name = "battery-level-100-charged-symbolic";
        else
            info.icon_name =
                "battery-level-%0.f%s-symbolic".printf(level, charging);

        // Fall back to the the coarse battery icons
        if (!Gtk.IconTheme.get_default().has_icon(info.icon_name))
            info.icon_name = device.icon_name;
    } else { // Unhandled device type
        info.icon_name = "battery-missing-symbolic"; // Battery w/ ?
        info.label = null;
    }

    var show_percent = settings.get_boolean("show-battery-percentage");
    info.panel_text = show_percent ? info.label : null;
    info.panel_tooltip = show_percent ? null : info.label;
}

async void create_battery_info(Panel.Signals sig) {
    // Obtain the proxy
    var device = yield Bus.get_proxy<Device?>(BusType.SYSTEM,
        "org.freedesktop.UPower", "/org/freedesktop/UPower/devices/DisplayDevice");
    if (device == null) return;

    // Create the info source
    var info = new Panel.CCInfo();
    info.panel_ordering = 5;
    info.bind_property("icon-name", info, "panel-icon-name");
    info.icon_name = "battery-missing-symbolic";
    info.label = "...";

    // Settings
    var settings = new Settings("org.gnome.desktop.interface");
    settings.changed["show-battery-percentage"].connect(() => {
        update_bat_info(info, device, settings);
    });

    // Whenever UPower reports changes, update the panel item
    device.g_properties_changed.connect((a, b) => {
        update_bat_info(info, device, settings);
    });

    // Populate the info
    update_bat_info(info, device, settings);

    // Register w/ the panel
    sig.register_cc_info(info, "gnome-power-panel.desktop");
}
