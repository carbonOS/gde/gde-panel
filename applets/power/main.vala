public Panel.Applet? create_applet(bool in_lock) {
    var sig = Panel.Signals.obtain();

    create_battery_info.begin(sig);
    create_brightness_slider.begin(sig);

    // We don't handle keyboard brightness anymore, but
    // g-s-d doesn't handle the brighness keys in carbonSHELL yet
    // so we need to deal with it here still
    // TODO: Drop this
    setup_legacy_kbd_signals(sig);

	return null; // We don't want a regular applet
}
