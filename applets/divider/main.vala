public Panel.Applet? create_applet(bool in_lock) {
    var applet = new Panel.Applet();
    applet.applet_name = "divider";
    applet.panel_item = new Gtk.Separator(Gtk.Orientation.VERTICAL);
	return applet;
}
