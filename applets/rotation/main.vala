[DBus(name="net.hadess.SensorProxy")]
interface SensorProxy : DBusProxy {
    public abstract bool has_accelerometer { get; }
}

private void update_rotation_toggle(Panel.CCToggle toggle, Settings settings) {
    toggle.icon_name = "rotation-%s-symbolic".printf(toggle.active ?
        "allowed" : "locked");
    settings.set_boolean("orientation-lock", !toggle.active);
}

public Panel.Applet? create_applet() {
    var sig = Panel.Signals.obtain();

    // Create the toggle
    var toggle = new Panel.CCToggle();
    toggle.name = "Rotation";
    toggle.status = null;
    toggle.visible = false; // Not visible by default
    sig.register_cc_toggle(toggle);

    // Bind the toggle to the gsetting
    var settings = new Settings("org.gnome.settings-daemon.peripherals.touchscreen");
    toggle.active = settings.get_boolean("orientation-lock");
    toggle.notify["active"].connect(() =>
        update_rotation_toggle(toggle, settings));
    update_rotation_toggle(toggle, settings);

    // Show/hide the toggle as appropriate
    Bus.watch_name(BusType.SYSTEM, "net.hadess.SensorProxy",
        BusNameWatcherFlags.NONE,
        (conn, name, owner) => { // Appeared
            SensorProxy proxy;
            try {
                proxy = conn.get_proxy_sync(name, "/net/hadess/SensorProxy");
                info("SensorProxy appeared");
            } catch {
                warning("Couldn't obtain SensorProxy");
                return;
            }

            toggle.visible = proxy.has_accelerometer;
            proxy.g_properties_changed.connect((props, inval) => {
                toggle.visible = proxy.has_accelerometer;
            });
        },
        (conn, name) => { // Vanished
            info("SensorProxy vanished");
            toggle.visible = false; // no sensor proxy = no autorotation
        });

    return null;
}
