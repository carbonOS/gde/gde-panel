class HomeButtonApplet : Panel.Applet {
	construct {
		applet_name = "home-btn";

		var btn = new Gtk.Button();
		var img = new Gtk.Image.from_resource("/csh/swipe-arrow-symbolic.svg");
		btn.add(img);
		img.pixel_size = 26;
		btn.get_style_context().add_class(Gtk.STYLE_CLASS_FLAT);
		btn.tooltip_text = "Home";
		btn.clicked.connect(() => {
		    warning("TODO: Mobile Home: Unimplemented");
		});
		panel_item = btn;
	}
}

public Panel.Applet? create_applet(bool in_lock) {
    if (in_lock)
        return null;
    else
	    return new HomeButtonApplet();
}
