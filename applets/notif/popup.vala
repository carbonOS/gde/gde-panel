class Notif.PopupWindow : Gtk.Window {
    private Gtk.Box notif_container;
    private NotificationWidget current_widget;
    private Settings placement_settings;
    private bool place_top;
    private Queue<Notification> notifs;

    private Gtk.Revealer revealer;
    private Hdy.Deck hide_slider;
    private Hdy.Deck dismiss_slider;

    private uint dismissal_timeout = 0;
    private uint window_close_timeout = 0;
    private uint show_timeout = 0;

    construct {
        // Set some basic properties
        this.width_request = 350;
        this.margin = 6;
        this.get_style_context().remove_class("background");
        notifs = new Queue<Notification>();
        placement_settings = new Settings("sh.carbon.gde-panel");

        // Setup layer shell
        GtkLayerShell.init_for_window(this);
        GtkLayerShell.set_layer(this, GtkLayerShell.Layer.OVERLAY);
        GtkLayerShell.set_namespace(this, "notification");
        GtkLayerShell.set_anchor(this, GtkLayerShell.Edge.RIGHT, true);
        GtkLayerShell.set_margin(this, GtkLayerShell.Edge.RIGHT, this.margin);
        GtkLayerShell.set_margin(this, GtkLayerShell.Edge.BOTTOM, this.margin);
        GtkLayerShell.set_margin(this, GtkLayerShell.Edge.LEFT, this.margin);
        GtkLayerShell.set_margin(this, GtkLayerShell.Edge.TOP, this.margin);

        // Hide whenever a popover opens
        Panel.Signals.obtain().on_hide_popovers.connect(not => dismiss());

        // Create the revealer
        revealer = new Gtk.Revealer();
        revealer.reveal_child = false;
        this.add(revealer);

        // Create the sliders
        hide_slider = new Hdy.Deck();
        hide_slider.transition_type = Hdy.DeckTransitionType.SLIDE;
        hide_slider.orientation = Gtk.Orientation.VERTICAL;
        hide_slider.notify["visible-child"].connect(() => {
            if (!hide_slider.transition_running &&
                hide_slider.visible_child != dismiss_slider &&
                revealer.child_revealed)
                dismiss();
        });
        hide_slider.notify["transition-running"].connect(() => {
            if (!hide_slider.transition_running &&
                hide_slider.visible_child != dismiss_slider &&
                revealer.child_revealed)
                dismiss();
        });
        revealer.add(hide_slider);
        dismiss_slider = new Hdy.Deck() {
            can_swipe_forward = false, // TODO: On mobile devices, should be true
            can_swipe_back = true,
            transition_type = Hdy.DeckTransitionType.SLIDE
        };
        dismiss_slider.notify["visible-child"].connect(() => {
            if (!dismiss_slider.transition_running &&
                dismiss_slider.visible_child != notif_container &&
                revealer.child_revealed)
                current_widget.notif.dismiss();
        });
        dismiss_slider.notify["transition-running"].connect(() => {
            if (!dismiss_slider.transition_running &&
                dismiss_slider.visible_child != notif_container &&
                revealer.child_revealed)
                current_widget.notif.dismiss();
        });
        notif_container = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);

        // Put the sliders inside of each other
        hide_slider.add(new Gtk.DrawingArea());
        hide_slider.add(dismiss_slider);
        hide_slider.add(new Gtk.DrawingArea());
        dismiss_slider.add(new Gtk.DrawingArea());
        dismiss_slider.add(notif_container);
        dismiss_slider.add(new Gtk.DrawingArea());

        // Configure the window position
        update_window_position();
        placement_settings.changed["place-top"].connect(update_window_position);
    }

    private void update_window_position() {
        place_top = placement_settings.get_boolean("place-top");

        revealer.transition_type = place_top ?
            Gtk.RevealerTransitionType.SLIDE_DOWN :
            Gtk.RevealerTransitionType.SLIDE_UP;

        hide_slider.can_swipe_forward = place_top;
        hide_slider.can_swipe_back = !place_top;

        GtkLayerShell.set_anchor(this, GtkLayerShell.Edge.TOP, place_top);
        GtkLayerShell.set_anchor(this, GtkLayerShell.Edge.BOTTOM, !place_top);
    }

    private void show_next() {
        if (show_timeout != 0) Source.remove(show_timeout);
        var was_visible = revealer.child_revealed;
        revealer.reveal_child = false;
        show_timeout = Timeout.add(was_visible ?
                                   revealer.transition_duration : 0,
                                   () => {
            show_timeout = 0;

            // Sometimes there's a race where widgets stick around
            // even after their controlling notif is destroyed
            foreach (var child in notif_container.get_children())
                child.destroy();

            // Make sure the sliders are set correctly;
            uint old_duration = hide_slider.transition_duration;
            hide_slider.transition_duration = 0;
            dismiss_slider.transition_duration = 0;
            hide_slider.visible_child = dismiss_slider;
            dismiss_slider.visible_child = notif_container;
            hide_slider.transition_duration = old_duration;
            dismiss_slider.transition_duration = old_duration;

            // Show the next notif, if there is one
            if (!notifs.is_empty()) {
                var notif = notifs.pop_head();
                current_widget = new NotificationWidget(notif, true);
                notif_container.add(current_widget);
                current_widget.show_all();
                revealer.reveal_child = true;

                // Start out timeout
                if (dismissal_timeout != 0)
                    Source.remove(dismissal_timeout);
                dismissal_timeout = Timeout.add(5000, on_dismissal_timeout);
            }

            return Source.REMOVE;
        });
    }

    private bool on_dismissal_timeout() {
        if (current_widget == null) {
            // If we don't have a widget for some reason, exit
            dismissal_timeout = 0;
            return Source.REMOVE;
        }

        if (current_widget.mouse_over) {
            // If the mouse is over the widget, reschedule dismissal
            dismissal_timeout = Timeout.add(1000, on_dismissal_timeout);
        } else {
            // If the mouse isn't over the widget, dismiss the widget
            remove_notification(current_widget.notif);
            dismissal_timeout = 0;
        }
        return Source.REMOVE;
    }

    public void show_notification(Notification notif) {
        // Check for open applets
        var applet_open = false;
        Panel.Applet[] applets = Panel.PluginLoader.obtain().get_applets();
        foreach (var applet in applets) {
            applet_open = applet.suppress_autohide;
            if (applet_open) break;
        }
        if (applet_open) { // Close and quit if an applet is open
            dismiss();
            return;
        }

        // Cancel an upcoming window close
        if (window_close_timeout != 0) {
            Source.remove(window_close_timeout);
            window_close_timeout = 0;
        }

        // Add the notification to the queue
        notifs.push_tail(notif);

        // Make sure the popup is visible
        if (!visible) this.show_all();

        // If we're not currently showing a notification, show the next one
        if (current_widget == null)
            show_next();
    }

    public void remove_notification(Notification notif) {
        // Remove this notification from the queue
        notifs.remove_all(notif);

        // If the current notification is visible, move on to the next
        if (current_widget != null)
            if (notif.id == current_widget.notif.id) {
                if (dismissal_timeout != 0) {
                    Source.remove(dismissal_timeout);
                    dismissal_timeout = 0;
                }
                show_next();
            }

        // Close the window
        if (notifs.is_empty()) dismiss();
    }

    public void dismiss() {
        if (dismissal_timeout != 0) {
            Source.remove(dismissal_timeout);
            dismissal_timeout = 0;
        }

        var was_visible = revealer.child_revealed;
        revealer.reveal_child = false;

        // Allow this to show again right away
        var widget_to_destroy = current_widget;
        current_widget = null;

        window_close_timeout = Timeout.add(was_visible ?
                                        revealer.transition_duration : 0,
                                        () => {
            window_close_timeout = 0;
            notifs.clear();
            visible = false;
            if (widget_to_destroy != null) widget_to_destroy.destroy();
            return Source.REMOVE;
        });
    }

    // Colors

    protected override void style_updated() {
        base.style_updated();
        Gtk.StyleContext ctx = this.get_style_context();
        Gtk.Settings settings = Gtk.Settings.get_default();

        bool dark = settings.gtk_application_prefer_dark_theme;
        if (!dark) dark = settings.gtk_theme_name.has_suffix("dark");
        if (!dark) {
            Gdk.RGBA rgba;
            var is_set = ctx.lookup_color("theme_bg_color", out rgba);
            dark = is_set && rgba.red + rgba.green + rgba.blue < 1.0;
        }

        var win_ctx = this.get_style_context();
        win_ctx.remove_class(dark ? "light" : "dark");
        win_ctx.add_class(dark ? "dark" : "light");
    }
}   
