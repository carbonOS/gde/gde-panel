private void update_dnd_toggle(Panel.CCToggle toggle) {
    toggle.icon_name = toggle.active ? "notifications-disabled-symbolic" :
        "notifications-symbolic";
}

public Panel.Applet? create_applet() {
    var settings = new Settings("org.gnome.desktop.notifications");
    var toggle = new Panel.CCToggle();
    toggle.name = "Do Not\nDisturb";
    toggle.status = null;
    toggle.multiline = true;
    toggle.more_settings = "gnome-notifications-panel.desktop";

    settings.bind("show-banners", toggle, "active", SettingsBindFlags.INVERT_BOOLEAN);
    toggle.notify["active"].connect(() => update_dnd_toggle(toggle));
    update_dnd_toggle(toggle);

    Panel.Signals.obtain().register_cc_toggle(toggle);
    return null;
}
