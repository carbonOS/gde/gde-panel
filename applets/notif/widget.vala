class Notif.NotificationWidget : Gtk.EventBox {
    public Notification notif;
    private uint time_elapsed_source = 0;

    // Widget
    private Gtk.Image app_icon;
    private Gtk.Label time_elapsed;
    private Gtk.Box action_row;

    public bool mouse_over { set; get; }

    public NotificationWidget(Notification notif, bool padded = false) {
        this.notif = notif;

        var root = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
        root.get_style_context().add_class("card");
        if (padded)
            root.get_style_context().add_class("padded");
        root.margin = 4;
        this.add(root);

        // Create the header container (app icon, app name, close action, time)
        var header = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 6);
        header.margin_start = header.margin_end = 6;
        root.pack_start(header, false);

        // Put app info in the header
        app_icon = new Gtk.Image();
        app_icon.pixel_size = 16;
        app_icon.no_show_all = true;
        notif.notify["app-icon"].connect(update_app_icon);
        notif.notify["app-color"].connect(update_app_icon);
        update_app_icon();

        header.pack_start(app_icon, false);
        var app_name = new Gtk.Label(null);
        notif.bind_property("app-name", app_name, "label",
            BindingFlags.SYNC_CREATE);
        header.pack_start(app_name, false);

        // Create the dismiss button
        var dismiss = new Gtk.Button.from_icon_name("edit-delete-symbolic");
        dismiss.get_style_context().add_class(Gtk.STYLE_CLASS_FLAT);
        dismiss.get_style_context().add_class("circular");
        dismiss.clicked.connect(() => notif.dismiss());
        var dismiss_revealer = new Gtk.Revealer() {
            transition_type = Gtk.RevealerTransitionType.SLIDE_LEFT,
            reveal_child = false
        };
        dismiss_revealer.add(dismiss);
        header.pack_end(dismiss_revealer, false);
        this.add_events(Gdk.EventMask.ENTER_NOTIFY_MASK |
                        Gdk.EventMask.LEAVE_NOTIFY_MASK |
                        Gdk.EventMask.TOUCH_MASK);
        // TODO: Whenever port to GTK4, use EventControllerMotion
        this.enter_notify_event.connect(evt => {
            if (evt.mode == Gdk.CrossingMode.NORMAL)
                dismiss_revealer.reveal_child = true;
            return Gdk.EVENT_PROPAGATE;
        });
        this.leave_notify_event.connect(evt => {
            if (evt.detail != Gdk.NotifyType.INFERIOR &&
                evt.detail != Gdk.NotifyType.ANCESTOR)
                dismiss_revealer.reveal_child = false;
            return Gdk.EVENT_PROPAGATE;
        });
        dismiss_revealer.bind_property("reveal-child", this, "mouse-over",
            BindingFlags.SYNC_CREATE);

        // Create the time elapsed label
        time_elapsed = new Gtk.Label("Now");
        time_elapsed.margin_end = 4;
        time_elapsed.halign = Gtk.Align.END;
        time_elapsed.get_style_context().add_class("dim-label");
        header.pack_end(time_elapsed, false);

        // Create the body of the notification
        var body_btn = new Gtk.Button();
        body_btn.get_style_context().add_class(Gtk.STYLE_CLASS_FLAT);
        body_btn.clicked.connect(() => { // Default action handling
            if (notif.default_action != null)
                notif.default_action.invoke();
            else if (notif.desktop_info != null) {
                Carbon.Utils.launch(notif.desktop_info);
                if (!notif.resident) notif.dismiss();
            }
        });
        root.pack_start(body_btn);
        var body_layout = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 6);
        body_btn.add(body_layout);

        // Create the title/body
        var text = new Gtk.Box(Gtk.Orientation.VERTICAL, 2);
        body_layout.pack_start(text, true);
        var title = new Gtk.Label(null);
        notif.bind_property("title", title, "label", BindingFlags.SYNC_CREATE,
            (binding, from, ref to) => {
                to = "<big>%s</big>".printf(Panel.fix_markup(from as string));
                return true;
            });
        title.use_markup = true;
        title.xalign = 0;
        title.wrap = true;
        title.wrap_mode = Pango.WrapMode.WORD;
        title.lines = 1;
        title.ellipsize = Pango.EllipsizeMode.END;
        text.pack_start(title);
        var body = new Gtk.Label(null);
        notif.bind_property("body", body, "label", BindingFlags.SYNC_CREATE,
            (binding, from, ref to) => {
                to = Panel.fix_markup(from as string);
                return true;
            });
        notif.notify["body"].connect(() => {
            body.visible = (notif.body != null && notif.body != "");
        });
        notif.notify_property("body");
        body.no_show_all = true;
        body.use_markup = true;
        body.xalign = 0;
        body.wrap = true;
        body.wrap_mode = Pango.WrapMode.WORD;
        body.lines = 2;
        body.ellipsize = Pango.EllipsizeMode.END;
        text.pack_start(body);

        // Create the icon
        var icon = new Gtk.Image();
        icon.pixel_size = 40;
        icon.no_show_all = true;
        icon.margin_end = 4;
        notif.bind_property("image", icon, "gicon", BindingFlags.SYNC_CREATE);
        icon.notify["gicon"].connect(() => {
            icon.visible = (icon.gicon != null);
        });
        icon.notify_property("gicon");
        body_layout.pack_start(icon, false);

        // Action row
        action_row = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 6);
        action_row.margin_top = 6;
        if (!padded) {
            action_row.margin_bottom = action_row.margin_left =
                action_row.margin_right = 6;
        }
        action_row.homogeneous = true; // Buttons should be equal size
        root.pack_start(action_row, false);
        update_actions();
        notif.notify_actions.connect(update_actions);

        // Start the time elapsed clock
        time_elapsed_source = Timeout.add_seconds(5, update_time_elapsed,
            Priority.LOW);
    }

    ~NotificationWidget() {
        if (time_elapsed_source > 0) Source.remove(time_elapsed_source);
    }

    private void update_app_icon() {
        /*
        if (notif.app_color != null && notif.app_icon is ThemedIcon) {
            var info = Gtk.IconTheme.get_default().lookup_by_gicon(
                notif.app_icon, app_icon.pixel_size,
                Gtk.IconLookupFlags.FORCE_SYMBOLIC);
            if (info != null) {
                app_icon.pixbuf = info.load_symbolic(notif.app_color);
                app_icon.visible = true;
                return;
            }
        }
        */
        app_icon.gicon = notif.app_icon;
        app_icon.visible = (notif.app_icon != null);
    }

    private void update_actions() {
        // Empty out the action row
        foreach (var child in action_row.get_children())
            child.destroy();

        // Update visibility of the action row
        var style_ctx = this.get_style_context();
        if (notif.actions.length > 0)
            style_ctx.add_class(Gtk.STYLE_CLASS_LINKED);
        else
            style_ctx.remove_class(Gtk.STYLE_CLASS_LINKED);

        // Add the action buttons
        foreach (var action in notif.actions) {
            if (action.action == "default") continue; // Skip default action
            var act_btn = new Gtk.Button.with_label(action.display);
            act_btn.clicked.connect(action.invoke);
            action_row.pack_start(act_btn);
        }
    }

    private bool update_time_elapsed() {
        var now = new DateTime.now();
        var elapsed = now.difference(notif.time_recieved);

        // Convert the time span to a string
        string result = "Now";
        if (elapsed >= TimeSpan.DAY) {
            result = "%llid".printf(elapsed / TimeSpan.DAY);
        } else if (elapsed >= TimeSpan.HOUR) {
            result = "%llih".printf(elapsed / TimeSpan.HOUR);
        } else if (elapsed >= TimeSpan.MINUTE) {
            result = "%llim".printf(elapsed / TimeSpan.MINUTE);
        } else if (elapsed >= TimeSpan.SECOND) {
            if (elapsed / TimeSpan.SECOND >= 30)
                result = "30s";
        }

        time_elapsed.label = result;
        return Source.CONTINUE;
    }
}

