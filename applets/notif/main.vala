class Notif.Applet : Panel.Applet {
    private Server srv;
    private HashTable<uint32, Gtk.Revealer> notifs;
    private PopupWindow popup;
    private Settings dnd_settings;

    // Widgets
    private Gtk.Label notif_counter;
    private Gtk.Stack switcher;
    private Gtk.Box notif_box_critical;
    private Gtk.Box notif_box_normal;
    private Gtk.Box notif_box_low;

    public bool banners_enabled { get; set; }

    construct {
        applet_name = "notif";
        srv = new Server();
        notifs = new HashTable<uint32, Gtk.Revealer>(null, null);
        popup = new PopupWindow();

        // Create the button
        var btn = Panel.create_button();
        var btn_layout = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 4);
        var btn_img = new Gtk.Image();
        btn_img.pixel_size = 18;
        btn_layout.add(btn_img);
        notif_counter = new Gtk.Label("");
        notif_counter.width_chars = 2;
        btn_layout.add(notif_counter);
        btn.add(btn_layout);
        this.panel_item = btn;

        // Bind the button to the state of do not distrurb
        dnd_settings = new Settings("org.gnome.desktop.notifications");
        //banners_enabled = dnd_settings.get_boolean("show-banners");
        //dnd_settings.bind("show-banners", notif_counter, "visible",
        //    SettingsBindFlags.DEFAULT);
        dnd_settings.bind("show-banners", this, "banners-enabled",
            SettingsBindFlags.DEFAULT);
        this.bind_property("banners-enabled", btn, "tooltip-text",
            BindingFlags.SYNC_CREATE, (_, from, ref to) => {
                to = (from.get_boolean()) ?
                    "Notifications" :
                    "Do Not Disturb Enabled";
                return true;
            });
        this.bind_property("banners-enabled", btn_img, "icon-name",
            BindingFlags.SYNC_CREATE, (_, from, ref to) => {
                to = (from.get_boolean()) ?
                    "notifications-symbolic" :
                    "notifications-disabled-symbolic";
                return true;
            });

        // Create the popover
        var popover = Panel.create_popover(this, btn);
		
		// Create the switcher that conditionally shows the empty state
		switcher = new Gtk.Stack();
		switcher.width_request = 350;
        switcher.transition_type = Gtk.StackTransitionType.CROSSFADE;
        popover.add(switcher);

        // Create the empty state
        var empty_state = new Dazzle.EmptyState();
        empty_state.pixel_size = 100;
        empty_state.icon_name = "emblem-ok-symbolic";
        empty_state.title = "No Notifications";
        empty_state.margin_top = 18;
        switcher.add_named(empty_state, "empty");

        // Create the main layout
        var layout = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
        var clear_button = new Gtk.Button.with_label("Clear All");
        clear_button.clicked.connect(clear_all);
        layout.pack_end(clear_button, false);
        var viewport = new Gtk.ScrolledWindow(null, null);
        viewport.hscrollbar_policy = Gtk.PolicyType.NEVER;
        viewport.max_content_height = 500;
        viewport.propagate_natural_height = true;
        layout.pack_start(viewport);
        var notif_box = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
        viewport.add(notif_box);
        switcher.add_named(layout, "main");

        // Add the boxes for the different priorities
        notif_box_critical = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
        notif_box.add(notif_box_critical);
        notif_box_normal = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
        notif_box.add(notif_box_normal);
        notif_box_low = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
        notif_box.add(notif_box_low);

        // Show everything
        switcher.show_all();

        // Connect the plumbing to the server
        foreach (var notif in srv.get_all())
            on_notif_added(notif);
        srv.ui_notif_added.connect(on_notif_added);
        srv.ui_notif_removed.connect(on_notif_removed);

        // Make sure notification counter and empty state are correct on startup
        update_notif_counter();
        update_empty_state();
    }

    private void on_notif_added(Notification notif) {
        update_notif_counter();
        update_empty_state();

        // Create notif widget
        var notif_widget = new NotificationWidget(notif);

        // Create the swipe
        var swipe = new Hdy.Deck() {
            can_swipe_back = true,
            can_swipe_forward = true,
            transition_type = Hdy.DeckTransitionType.SLIDE
        };
        swipe.notify["visible-child"].connect(() => {
            if (!swipe.transition_running &&
                swipe.visible_child != notif_widget)
                notif.dismiss();
        });
        swipe.notify["transition-running"].connect(() => {
            if (!swipe.transition_running &&
                swipe.visible_child != notif_widget)
                notif.dismiss();
        });
        swipe.add(new DismissSwipe(true));
        swipe.add(notif_widget);
        swipe.add(new DismissSwipe(false));
        swipe.visible_child = notif_widget;

        // Create wrapper animation
        Gtk.Revealer anim = new Gtk.Revealer();
        anim.transition_type = Gtk.RevealerTransitionType.SLIDE_DOWN;
        anim.transition_duration = 500;
        anim.add(swipe);

        // Position the notification based on urgency
        notif.notify["urgency"].connect(() => {
            // Remove from old parent
            var old_parent = anim.get_parent();
            if (old_parent != null) old_parent.remove(anim);

            // Put the notification at the top of its urgency box
            switch (notif.urgency) {
                case 2:
                    notif_box_critical.pack_end(anim, false);
                    break;
                case 1:
                    notif_box_normal.pack_end(anim, false);
                    break;
                default:
                    notif_box_low.pack_end(anim, false);
                    break;
            }

            // Ensure it is visible
            anim.show_all();
        });
        notif.notify_property("urgency"); // Fix bug where urgency isn't set

        // If do not distrub is off
        if (banners_enabled) {
            // Play the sound
            // TODO

            // Show the popup, if urgency isn't low
            popup.show_notification(notif);
        }

        // Show the notification
        notifs.replace(notif.id, anim); // Add the widget to the list
        notif.thaw_notify(); // Unfreeze the notif; populate the gui
        anim.reveal_child = true; // Start the show animation
        notif.start_timeout(); // Start notification's timeout (self-dismiss)
    }

    private void on_notif_removed(Notification notif) {
        // Potentially remove the notification from being popped up
        popup.remove_notification(notif);

        // Update the various state
        update_notif_counter();
        var empty = update_empty_state();

        // Destroy the widget
        var widget = notifs.lookup(notif.id);
        widget.reveal_child = false; // Start hide animation
        Timeout.add(empty ? 0 : widget.transition_duration, () => {
            widget.destroy();
            notifs.remove(notif.id);
            return Source.REMOVE;
        });
    }

    private void update_notif_counter() {
        int count = srv.get_n_notifications();
        if (count > 99)
            notif_counter.label = "∞";
        else
            notif_counter.label = "%d".printf(count);
    }

    private bool update_empty_state() {
        int count = srv.get_n_notifications();
        if (count > 0) {
            switcher.visible_child_name = "main";
            return false;
        } else {
            switcher.visible_child_name = "empty";
            return true;
        }
    }

    private void clear_all() {
        foreach (var notif in srv.get_all()) notif.dismiss();
    }
}

class Notif.DismissSwipe : Gtk.Image {
    public DismissSwipe(bool left) {
        icon_name = "edit-delete-symbolic";
        xalign = left ? 1.0f : 0.0f;
        if (left)
            margin_end = 6;
        else
            margin_start = 6;
        margin_top = margin_bottom = 4;
        this.get_style_context().add_class("dismiss-swipe");
    }
}

public Panel.Applet? create_applet() {
	return new Notif.Applet();
}
