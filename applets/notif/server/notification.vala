public class Notif.Notification : Object {
    // Basics
    public uint32 id { set; get; }
	public string title { set; get; }
	public string body { set; get; }
	public int32 timeout { set; get; }
	public DateTime time_recieved { set; get; }

	// Actions
	public Action[] actions;
	public Action? default_action { set; get; }
	public signal void notify_actions();

    // Hints
	public bool resident { set; get; }
	public uint8 urgency { set; get; }

    // App info
    public DesktopAppInfo? desktop_info { set; get; }
	public string app_name { set; get; }
	public Icon app_icon { set; get; }
	public string app_icon_str { set; get; }
	public Gdk.RGBA? app_color { set; get; }

    // Image
    public Icon image { set; get; }

	// Sound
	public string? sound_file { set; get; }
	public string sound_name { set; get; }
	public bool suppress_sound { set; get; }

    // Internal state
    public Server server; // Reference back to the server
    public BusName sender; // Bus peer that sent the notification
    public uint process_watch_name = 0;
    public uint? action_dismiss_idle = null;
	private uint? timeout_id = null;

	// Actions

	public Action? get_action(string action) {
		foreach (Action act in this.actions)
			if (act.action == action)
			    return act;
		return null;
	}

	public void invoke_action(string action) {
		this.get_action(action).invoke();
	}

	// Dismiss and timeout

	public void start_timeout() {
	    // Cancel any existing timeout
		if (timeout_id != null) Source.remove(timeout_id);

        // Handle cases where we pick a timeout
		var real_timeout = timeout;
		if (real_timeout == -1) // We have the freedom to pick a timeout
		    if (urgency == 2) // Never time out critical notifications
		        real_timeout = 0;
		    else if (urgency == 1) // Time out normal notifs in 1 hr
		        real_timeout = 60 * 60 * 1000;
		    else // Time out low priority notifs in 5 minutes
		        real_timeout = 5 * 60 * 1000;

        // If timeout is disabled, do nothing
		if (real_timeout == 0) return;

        // Schedule this dismissal
		timeout_id = Timeout.add(real_timeout, () => {
		    timeout_id = null;
		    this.dismiss(CloseReason.EXPIRED);
			return Source.REMOVE;
		});
	}

    // Restart the timeout if it was already running
    // Used in server's replacement logic
	public void restart_timeout() {
	    if (timeout_id != null)
	        start_timeout();
	}

	public void dismiss(int reason = CloseReason.DISMISSED) {
	    if (timeout_id != null) Source.remove(timeout_id);
		server.notification_closed(this.id, reason);
	}
}

public class Notif.Action : Object {
    public string action { set; get; }
	public string display { set; get; }
	public Notification notif;

	public void invoke() {
        notif.server.action_invoked(notif.id, this.action);

        // Schedule dismiss when idle
        // This gives clients time to replace the notification on action
        if (!notif.resident)
            notif.action_dismiss_idle = Idle.add(() => {
                notif.dismiss();
                return Source.REMOVE;
            });
	}
}
