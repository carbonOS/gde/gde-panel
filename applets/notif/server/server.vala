[DBus (name = "org.freedesktop.Notifications")]
public class Notif.Server : Object {

	public Server() {
	    Bus.own_name(
	        BusType.SESSION, "org.freedesktop.Notifications",
	        BusNameOwnerFlags.REPLACE | BusNameOwnerFlags.ALLOW_REPLACEMENT,
	        con => {
	            try {
	                con.register_object("/org/freedesktop/Notifications", this);
	            } catch (Error e) {
	                error("Notif: %s", e.message);
	            }
	        }, null, () => warning("Notif: Failed to obtain bus name")
	    );

		this.notifications = new GenericArray<Notification>();
		notification_closed.connect(handle_notification_closed);
	}

	// NOTIFICATION MANAGEMENT

	private GenericArray<Notification> notifications;
    private uint32 last_id = 1;

    [DBus (visible = false)]
	public int get_n_notifications() {
        return notifications.length;
	}

	[DBus (visible = false)]
	public Notification? get_notification(uint32 id) {
		foreach (Notification notif in get_all())
		    if (notif.id == id)
		        return notif;
		return null;
	}

	[DBus (visible = false)]
	public void handle_notification_closed(uint32 id, uint32 reason) {
		var notification = this.get_notification(id);
		if (notification != null) {
		    if (notification.process_watch_name != 0)
		        Bus.unwatch_name(notification.process_watch_name);
			notifications.remove(notification);
			ui_notif_removed(notification);
		}
	}

	private uint32 generate_new_id() {
	    // If it rolls over, we'll start to have collisions
	    // Luckily, this will happen at around 4.3 trillion notifications,
	    // so I think this is good-enough behavior
	    // It's also endorsed by the spec :)
		if (last_id == uint32.MAX)
		    last_id = 1;

        last_id++;
        return last_id;
	}

    // Dismisses n whenever the sender disappears from the bus
	private static uint register_watch_name(BusName sender, Notification n) {
        return Bus.watch_name(BusType.SESSION, sender, BusNameWatcherFlags.NONE,
            null, (connection, name) => {
                n.dismiss(CloseReason.UNDEFINED);
            });
	}

	[DBus (visible = false)]
	public Notification[] get_all() {
		return notifications.data;
	}

    // Signals to communicate notifications added/removed to the GUI
	[DBus (visible = false)]
	public signal void ui_notif_added(Notification notif);
	[DBus (visible = false)]
	public signal void ui_notif_removed(Notification notif);

	// PROTOCOL

	public string[] get_capabilities() throws GLib.Error {
		string[] abilities = {};
		abilities += "body";
		abilities += "body-markup";
		abilities += "icon-static";
		abilities += "actions";
		abilities += "sound";
		return abilities;
	}

	// NOTE: Capital name here so it doesn't conflict with GObject's notify.
	public uint32 Notify(string app_name,
						 uint32 replaces_id,
						 string app_icon,
					     string summary,
						 string body,
						 string[] actions,
						 HashTable<string, Variant> hints,
						 int32 expire_timeout,
						 BusName sender) throws GLib.Error {
		Notification n; // The notification we're going to be manipulating

        if (replaces_id != 0 && get_notification(replaces_id) == null)
            replaces_id = 0;
        if (replaces_id == 0) { // We have a new notification here
            n = new Notification();
            n.id = generate_new_id();
            n.sender = sender;
            n.server = this;
            n.freeze_notify();
            notifications.add(n);
            ui_notif_added(n);
        } else { // We're updaing an existing notification
            n = get_notification(replaces_id);
            if (n.sender != sender)
                return replaces_id; // There's foul play here. Just drop it
            if (n.action_dismiss_idle != null)
                Source.remove(n.action_dismiss_idle);
        }

		// Populate basic info
		n.title = summary;
		n.body = body;
		n.time_recieved = new DateTime.now();

        // Handle notification timeout
        n.timeout = expire_timeout;
		if (n.timeout > 0 && n.timeout < 2000) // If timeout is < 2s, make it 2s
		    n.timeout = 2000;
		if (replaces_id != 0) // Restart the timer if replacing
		    n.restart_timeout();

		// Populate the actions
		Action? action = null;
		Action[] tmp = {}; // Needed b/c can't do += to a public array
		foreach (var str in actions) {
		    if (action == null) {
		        action = new Action();
		        action.action = str;
		        action.notif = n;

		        if (str == "default")
		            n.default_action = action;
		    } else {
	            action.display = str;
	            tmp += action;
		        action = null;
		    }
		}
		n.actions = tmp;
		n.notify_actions();

        // Deal with hints
		n.resident = hints.lookup("resident") as bool ?? false; // Don't close on action
		n.urgency = hints.lookup("urgency") as uint8 ?? 1; // Importance of the notification

        // App name & icon
        // Use whatever information we have to get the best app info we can
        var sender_pid = Panel.get_dbus_pid_from_sender(sender);
        var flatpak_appid = Panel.get_flatpak_appid_from_pid(sender_pid);
        var appid = flatpak_appid ?? (hints.lookup("desktop-entry") as string);
        if (appid != null) { // We resolved the app
            var fixed_appid = Panel.fix_app_id(appid);
            n.desktop_info = new DesktopAppInfo(fixed_appid + ".desktop");
            n.app_color = Panel.extract_app_color(n.desktop_info);
            n.app_name = n.desktop_info.get_name();
            n.app_icon = n.desktop_info.get_icon();
            if (n.app_icon is ThemedIcon) { // Try to make it symbolic
                var themed = n.app_icon as ThemedIcon;
                themed.prepend_name(themed.names[0] + "-symbolic");
            }
        } else { // We couldn't resolve the app; fall back to provided info
            n.desktop_info = null;
            n.app_color = null;
            n.app_name = (app_name == "") ? "Unknown" : app_name;
            n.app_icon = (app_icon == "") ? null :
		        new ThemedIcon.from_names({app_icon + "-symbolic", app_icon});
        }

        // Load/handle image data
		var image_data = hints.lookup("image-data") ??
		                 hints.lookup("image_data") ?? // Legacy hint
		                 hints.lookup("icon_data"); // Legacy hint
		var image_path = hints.lookup("image-path") ??
		                 hints.lookup("image_path"); // Legacy hint
	    if (image_data != null) { // Load an image directly from the variant
            var iter = image_data.iterator();

            int width = -1, height = -1, rowstride = -1,
                bits_per_sample = -1, channels = -1;
            bool has_alpha = false;
            uint8 byte = 0;
            uint8[] data = {};
            iter.next("i", &width);
            iter.next("i", &height);
            iter.next("i", &rowstride);
            iter.next("b", &has_alpha);
            iter.next("i", &bits_per_sample);
            iter.next("i", &channels);

            var array_iter = iter.next_value().iterator();
            while (array_iter.next("y", &byte))
                data += byte;

            n.image = new Gdk.Pixbuf.from_data(data, Gdk.Colorspace.RGB,
                has_alpha, bits_per_sample, width, height, rowstride);
	    } else if (image_path != null) { // Load an image from a path/icon name
            var path = image_path.get_string();
            if (path.has_prefix("file://")) {
                var fs_path = Uri.unescape_string(path).replace("file://", "");
                n.image = new Gdk.Pixbuf.from_file(fs_path);
            } else {
                n.image = new ThemedIcon(path);
            }
	    } else // We don't have an image
	        n.image = null;

        // Sound
        n.sound_file = hints.lookup("sound-file") as string?;
        n.sound_name = hints.lookup("sound-name") as string ?? "message";
        n.suppress_sound = hints.lookup("suppress-sound") as bool ?? false;

        // Set up a listener that triggers dismiss when sender exits
        var sender_comm = Panel.get_comm_from_pid(sender_pid);
        switch (sender_comm) {
            case "notify-send":
                break; // Exempted from dismissing on exit
            default:
                if (n.process_watch_name == 0)
                    n.process_watch_name = register_watch_name(sender, n);
                break;
        }

		// Return the notification's ID to the sender
		return n.id;
	}

	public void close_notification(uint32 id) throws GLib.Error {
		var notif = get_notification(id);
		if (notif != null)
		    notif.dismiss(CloseReason.SELF);
	}

	public void get_server_information(out string name, out string vendor,
	        out string version, out string spec_version) throws GLib.Error {
		name = "graphite-notif-server";
		vendor = "carbonOS";
		version = "1";
		spec_version = "1.2";
	}

	public signal void notification_closed(uint32 id, uint32 reason);

	public signal void action_invoked(uint32 id, string action_key);
}

public enum CloseReason {
	EXPIRED = 1,
	DISMISSED = 2,
	SELF = 3,
	UNDEFINED = 4
}
