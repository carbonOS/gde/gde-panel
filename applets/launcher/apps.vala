class AppsGrid : Gtk.ScrolledWindow {
    Gtk.FlowBox apps_container;
    Cancellable current_cancellable;

    construct {
        this.hscrollbar_policy = Gtk.PolicyType.NEVER;

		apps_container = new Gtk.FlowBox();
		apps_container.selection_mode = Gtk.SelectionMode.NONE;
		apps_container.orientation = Gtk.Orientation.HORIZONTAL;
		apps_container.homogeneous = true;
		apps_container.set_sort_func((a, b) => { // Sort by app name
		    string a_name = (a as AppGridItem).app.get_name();
		    string b_name = (b as AppGridItem).app.get_name();
		    return a_name.collate(b_name);
		});
		this.add(apps_container);
		apps_container.show_all();

		// Edit the viewport's valign
		this.get_child().valign = Gtk.Align.START;
    }
    
    private async List<AppInfo> enumerate_apps() {
        SourceFunc callback = enumerate_apps.callback;
        var t = new Thread<List<AppInfo>>("enumerate-apps", () => {
            List<AppInfo> output = AppInfo.get_all();
            Idle.add((owned) callback);
            return output;
        });
        yield; // Let the thread run in the background
        return t.join();
    }

    private async void do_invalidate(Cancellable cancellable) {
        List<AppInfo> apps = yield enumerate_apps();
        if (cancellable.is_cancelled()) return;

        // Create the widgets
        foreach (AppInfo app in apps) { // Add in the new ones
            // Check if we haven't been cancelled since
		    if (cancellable.is_cancelled()) return;

            // Check if we should skip this
            if (!app.should_show()) continue;

            // Add the icon
            var item = new AppGridItem((DesktopAppInfo) app, cancellable);
		    apps_container.add(item);
		    item.show_all();

		    // Wait until idle to add next app
		    Idle.add(do_invalidate.callback);
		    yield;
		}
    }

    public void invalidate() {
        current_cancellable.cancel(); // Mark that we should stop running
        current_cancellable = new Cancellable(); // Create a new cancellable
        do_invalidate.begin(current_cancellable); // Start invalidating
    }

}

class AppGridItem : Gtk.FlowBoxChild {
    public DesktopAppInfo app;

    public AppGridItem(DesktopAppInfo app, Cancellable? cancellable = null) {
        if (cancellable != null) cancellable.connect(this.destroy);

        this.can_focus = false; // Only allow focus on the button

		Gtk.Box app_info = new Gtk.Box(Gtk.Orientation.VERTICAL, 8);
		Gtk.Image icon = new Gtk.Image.from_gicon(app.get_icon(), Gtk.IconSize.INVALID);
		icon.pixel_size = 52;
		icon.get_style_context().add_class("icon-dropshadow");
		app_info.add(icon);
		Gtk.Label name = new Gtk.Label(app.get_name());
		name.ellipsize = Pango.EllipsizeMode.END;
		name.lines = 1;
		name.margin_left = name.margin_right = 10;
		name.max_width_chars = 10;
		app_info.add(name);

		// Launcher button
		Gtk.Button launcher = new Gtk.Button();
		launcher.width_request = 150;
		launcher.get_style_context().add_class(Gtk.STYLE_CLASS_FLAT);
		launcher.add(app_info);
		launcher.clicked.connect(launch);

		// Details tooltip
		string? desc = app.get_description();
		if (desc != null && desc.length > 0) {
		    desc = desc.replace("&", "&amp;");
		    desc = @"\n$desc";
		} else desc = "";
		launcher.tooltip_markup = @"<b>$(app.get_display_name ())</b>$desc";
        
        this.app = app;
        this.add(launcher);

        // Menu
        Gtk.Menu menu = new Gtk.Menu();

        // The standard options that are defined in the app info
        string[] actions = app.list_actions();
        foreach (string action in actions) {
            string action_disp = app.get_action_name(action);
            Gtk.MenuItem menu_action_item = new Gtk.MenuItem.with_label(action_disp);
            menu_action_item.activate.connect(() => {
                Carbon.Utils.launch(app, action);
            });
            menu.append(menu_action_item);
        }
        if (actions.length > 0) menu.append(new Gtk.SeparatorMenuItem());

        // Pin handling
        Gtk.MenuItem menu_pin = new Gtk.MenuItem();
        var pin_settings = new Settings("sh.carbon.gde-panel");
        var pin_id = app.get_id().replace(".desktop", "");
        var pinned = pin_id in pin_settings.get_strv("pinned-apps");
        menu_pin.label = pinned ? "Unpin" : "Pin";
        menu_pin.activate.connect(() => {
            string[] pinned_apps = pin_settings.get_strv("pinned-apps");
            if (!pinned) {
                pinned_apps += pin_id;
                pin_settings.set_strv("pinned-apps", pinned_apps);
                pinned = true;
            } else {
                string[] new_value = {};
                foreach (var id in pinned_apps)
                    if (id != pin_id) new_value += id;
                pin_settings.set_strv("pinned-apps", new_value);
                pinned = false;
            }
            menu_pin.label = pinned ? "Unpin" : "Pin";
        });
        menu.append(menu_pin);

        // Edit option to modify the desktop entry
        Gtk.MenuItem menu_edit = new Gtk.MenuItem.with_label("Edit");
        menu_edit.activate.connect(() => {
            Process.spawn_command_line_async("gde-edit-app-dialog " + app.get_id());
        });
        menu.append(menu_edit);

        // App info option to open settings
        Gtk.MenuItem menu_app_info = new Gtk.MenuItem.with_label("App Settings");
        menu_app_info.activate.connect(() => {
            Bus.@get.begin(BusType.SESSION, null, (obj, res) => {
                var connection = Bus.@get.end(res);
                var remote_actions = DBusActionGroup.@get(connection,
                    "org.gnome.ControlCenter", "/org/gnome/ControlCenter");
                VariantBuilder builder = new VariantBuilder(
                    new VariantType("(sav)"));
                builder.add("s", "applications");
                builder.open(new VariantType("av"));
                builder.add("v", new Variant.string(app.get_id()));
                builder.close();
                remote_actions.activate_action("launch-panel", builder.end());
            });
        });
        menu.append(menu_app_info);

        // Make the menu pop up on right-click
        menu.attach_to_widget(launcher, null);
        menu.show_all();
        launcher.button_press_event.connect(evt => {
            if (evt.button == Gdk.BUTTON_SECONDARY) menu.popup_at_pointer(evt);
            return Gdk.EVENT_PROPAGATE;
        });

		// Drag and drop
		Gtk.drag_source_set(launcher, Gdk.ModifierType.BUTTON1_MASK, null, Gdk.DragAction.COPY);
		Gtk.drag_source_add_uri_targets(launcher);
		launcher.drag_begin.connect(ctx => Gtk.drag_set_icon_gicon(ctx, app.get_icon(), 0, 0));
		launcher.drag_data_get.connect((ctx, data, info, time) => data.set_uris({ @"file://$(app.get_filename ())" }));
    }

    public void launch() {
        Carbon.Utils.launch(app);
    }
}
