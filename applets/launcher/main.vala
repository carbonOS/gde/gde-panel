class LauncherApplet : Panel.Applet {
    private AppInfoMonitor app_monitor = AppInfoMonitor.@get();

	construct {
		applet_name = "launcher";

		var btn = Panel.create_img_button("view-app-grid-symbolic",
            "Applications", 24);
        var popover = Panel.create_popover(this, btn);
        popover.width_request = 700;
        popover.height_request = 500;

		var layout = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
        var switcher = new Gtk.Stack();
        switcher.transition_type = Gtk.StackTransitionType.SLIDE_UP_DOWN;

        // Add search
        var search = new SearchList();
        switcher.add_named(search, "search");

        // Add the apps grid
        var apps = new AppsGrid();
        bool should_invalidate = false;
        apps.invalidate();
        app_monitor.changed.connect(() => {
            if (popover.visible)
                apps.invalidate();
            else
                should_invalidate = true;
        });
        popover.show.connect(() => {
            if (should_invalidate) {
                should_invalidate = false;
                apps.invalidate();
            }
        });
        switcher.add_named(apps, "apps");

        // Make "apps" visible by default
        switcher.show_all();
        switcher.visible_child_name = "apps";

        var search_box = new Gtk.SearchEntry();
        search_box.margin_start = 2;
        search_box.margin_end = 2;
        search_box.margin_top = 2;
        search_box.search_changed.connect(() => {
            if (search_box.text != "") {
                switcher.visible_child_name = "search";
                search.search(search_box.text);
            } else {
                switcher.visible_child_name = "apps";
                search.cancel();
            }
        });
        search_box.stop_search.connect(() => {
            search_box.text = "";
            switcher.visible_child_name = "apps";
            search.cancel();
        });
        app_monitor.changed.connect(() => {
           search.on_settings_changed();
        });
        search_box.activate.connect(() => {
            search.launch_first_result();
            popover.popdown();
        });
        search.request_return_to_apps.connect(() => {
            search_box.stop_search();
            search_box.grab_focus_without_selecting();
        });
        popover.closed.connect(() => {
            search_box.text = "";
        });

        layout.pack_start(search_box, false);
        layout.pack_start(switcher);
	    layout.show_all();
		popover.add(layout);

		panel_item = btn;
	}
}

public Panel.Applet? create_applet(bool in_lock) {
	return new LauncherApplet();
}
