const string GROUP = "Shell Search Provider";

class SearchList : Gtk.ScrolledWindow {
    Gtk.Box app_results_card;
    Gtk.FlowBox app_results;
    Gtk.Label app_results_no_results;
    Gtk.FlowBox remote_results_view;

    List<SearchProvider> providers;
    bool should_reload_providers = false;
    ListStore results;
    bool in_search = false;
    Cancellable cancellable = null;

	Settings search_settings = new Settings("org.gnome.desktop.search-providers");
    public string[] sort_order;

    construct {
        this.hscrollbar_policy = Gtk.PolicyType.NEVER;
        
        Gtk.Box layout = new Gtk.Box(Gtk.Orientation.VERTICAL, 8);
        layout.margin_top = 8;
        layout.margin_bottom = 8;

        app_results_card = new Gtk.Box(Gtk.Orientation.VERTICAL, 6);
        app_results_card.get_style_context().add_class("card");
        app_results_card.margin_start = 8;
        app_results_card.margin_end = 8;
        layout.pack_start(app_results_card, false);

        Gtk.Button app_header_btn = new Gtk.Button();
        app_header_btn.get_style_context().add_class("flat");
        app_header_btn.clicked.connect(() => request_return_to_apps());
        Gtk.Box app_header_layout = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 8);
        Gtk.Image app_header_icon = new Gtk.Image.from_icon_name(
            "view-app-grid-symbolic", Gtk.IconSize.INVALID);
        app_header_icon.pixel_size = 20;
        app_header_layout.pack_start(app_header_icon, false, false);
        app_header_layout.add(new Gtk.Label("Apps"));
        Gtk.Image app_header_arrow = new Gtk.Image.from_icon_name("go-next-symbolic",
            Gtk.IconSize.BUTTON);
        app_header_layout.pack_end(app_header_arrow, false, false);
        app_header_btn.add(app_header_layout);
        app_results_card.pack_start(app_header_btn, false, false);

        app_results_no_results = new Gtk.Label("No results\n" +
            "Advanced search has been disabled in <a href=\"\">Settings</a>");
        app_results_no_results.use_markup = true;
        app_results_no_results.justify = Gtk.Justification.CENTER;
        app_results_no_results.track_visited_links = false;
        app_results_no_results.no_show_all = true;
        app_results_no_results.margin_top = 24;
        app_results_no_results.margin_bottom = 24;
        app_results_no_results.get_style_context().add_class(
            Gtk.STYLE_CLASS_DIM_LABEL);
        app_results_no_results.activate_link.connect(uri => {
            Carbon.Utils.launch(new DesktopAppInfo(
                "gnome-search-panel.desktop"), null);
        });
        app_results_card.add(app_results_no_results);

        app_results = new Gtk.FlowBox();
		app_results.selection_mode = Gtk.SelectionMode.NONE;
		app_results.orientation = Gtk.Orientation.HORIZONTAL;
		app_results.homogeneous = true;
		app_results_card.add(app_results);

        remote_results_view = new Gtk.FlowBox();
        remote_results_view.max_children_per_line = 1;
        remote_results_view.row_spacing = 8;
        remote_results_view.selection_mode = Gtk.SelectionMode.NONE;
        results = new ListStore(typeof(SearchResult));
        remote_results_view.bind_model(results, obj => new SearchResultView(obj as SearchResult));
        layout.pack_start(remote_results_view, false);

        this.add(layout);

        // Deal with settings/loading search providers
        sort_order = search_settings.get_strv("sort-order");
        search_settings.changed["sort-order"].connect(() => {
            sort_order = search_settings.get_strv("sort-order");
            remote_results_view.invalidate_sort();
        });
        search_settings.changed.connect(on_settings_changed);
        load_search_providers();


        cancel(); // Put into a state ready for the next search
    }
    
    public void on_settings_changed() {
        sort_order = search_settings.get_strv("sort-order");
        if (in_search)
            should_reload_providers = true;
        else
            load_search_providers();
    }

    public void search(string query) {
        // Remove the previous app results
        foreach (Gtk.Widget child in app_results.get_children())
            child.destroy();

        // Search for apps
        bool external_disabled = search_settings.get_boolean("disable-external");
        int count = 0;
        foreach (string** group in DesktopAppInfo.search(query)) {
            for (int i = 0; i < strv_length((string[]) group) &&
                    (external_disabled || count < 8); i++) {
                DesktopAppInfo app = new DesktopAppInfo(group[i]);
                if (app == null) continue;
                if (!app.should_show() && app.get_string("X-SearchOnly") != "true")
		            continue; // Skip this one
                app_results.add(new AppGridItem(app, cancellable));
                count++;
            }
            if (!external_disabled && count >= 8) break;
        }
        app_results.show_all();
        app_results_card.visible = external_disabled || (count > 0);
        app_results_no_results.visible = external_disabled && count == 0;

        // Search through the search providers
        foreach (SearchProvider provider in providers)
            provider.search.begin(query, results, cancellable);

        in_search = true;
    }

    public void launch_first_result() {
        AppGridItem? first_app = app_results.get_child_at_index(0) as AppGridItem;
        SearchResultView? first_result = remote_results_view.get_child_at_index(0) as SearchResultView;
        if (first_app != null) {
            (first_app as AppGridItem).launch();
        } else if (first_result != null) {
            first_result.launch_first_subresult();
        } else return;
        cancel();
    }

    public void cancel() {
        cancellable.cancel();
        cancellable = new Cancellable();
        in_search = false;

        results.remove_all();
        app_results_card.hide();

        if (should_reload_providers) {
            should_reload_providers = false;
            load_search_providers();
        }
    }

    public signal void request_return_to_apps();

    private void load_search_providers() {
        providers = new List<SearchProvider>();
        List<string> ids = new List<string>();

        if (search_settings.get_boolean("disable-external"))
            return;

        string[] disabled = search_settings.get_strv("disabled");
        string[] enabled = search_settings.get_strv("enabled");
        foreach (string data_dir in Environment.get_system_data_dirs()) {
            string search_path = data_dir + "/gnome-shell/search-providers";
            Dir dir;
            try {
                dir = Dir.open(search_path);
            } catch (FileError e) {
                continue;
            }
            string? name = null;
            while ((name = dir.read_name()) != null) {
                KeyFile keyfile = new KeyFile();
                try {
                    keyfile.load_from_file(search_path + "/" + name, KeyFileFlags.NONE);
                    if (!keyfile.has_group(GROUP)) continue;
                    int version = keyfile.get_integer(GROUP, "Version");
                    if (version != 2) continue; // We only support version 2 at the moment
                    string desktop_id = keyfile.get_string(GROUP, "DesktopId");
                    if (ids.find_custom(desktop_id, strcmp) != null) continue; // Load each app once only
                    ids.append(desktop_id);

                    bool default_disabled = false;
                    if (keyfile.has_key(GROUP, "DefaultDisabled"))
                        default_disabled = keyfile.get_boolean(GROUP,
                            "DefaultDisabled");
                    if (desktop_id in disabled ||
                        (default_disabled && !(desktop_id in enabled)))
                        continue;
                    string bus_name = keyfile.get_string(GROUP, "BusName");
                    string object_path = keyfile.get_string(GROUP, "ObjectPath");
                    DesktopAppInfo app = new DesktopAppInfo(desktop_id);
                    if (app == null) {
                        warning("Search provider %s claims an invalid app: %s", name, desktop_id);
                        continue;
                    }
                    Bus.get_proxy.begin<DBusSearchInterface>(BusType.SESSION, bus_name, object_path, DBusProxyFlags.NONE, null, (obj, res) => {
                        DBusSearchInterface proxy = Bus.get_proxy.end(res);
                        providers.append(new SearchProvider(app, proxy, this));
                        info("Loaded search provider for %s", desktop_id);
                    });
                } catch (Error e) {
                    warning("Failed to load search provider %s: %s\n", name, e.message);
                    continue;
                }
            }
        }
    }

    public int result_sort_func(Object a, Object b) {
        var result1 = a as SearchResult;
        var result2 = b as SearchResult;

        int index1 = -1;
        int index2 = -1;
        for (int i = 0; i < sort_order.length; i++) {
            string entry = sort_order[i];
            if (entry == result1.app.get_id()) {
                index1 = i;
                if (index2 != -1) break;
            } else if (entry == result2.app.get_id()) {
                index2 = i;
                if (index1 != -1) break;
            }
        }

        if (index1 != -1 && index2 == -1) return -1;
        else if (index1 == -1 && index2 != -1) return 1;
        else if (index1 == -1 && index2 == -1) return result1.app.get_name().collate(result2.app.get_name());
        else return index1 - index2;
    }
}

class SearchProvider {
    string? previous_query = null;
    string[]? result_ids = null;
    DBusSearchInterface proxy;
    DesktopAppInfo app;
    SearchResult? result = null;
    HashTable<string, SearchSubResult> meta_cache;
    bool appended = false;
    SearchList list; // Used for sort order

    public SearchProvider(DesktopAppInfo app, DBusSearchInterface proxy, SearchList list) {
        this.app = app;
        this.proxy = proxy;
        this.list = list;
    }

    public async void search(string query, ListStore output,
            Cancellable cancellable) {
        cancellable.connect(end_search);

        // Ask the proxy to do the search
        if (result_ids == null || previous_query == null ||
                query.index_of(previous_query) != 0) {
            result_ids = yield proxy.get_initial_result_set(query.split(" "));
        } else {
            result_ids = yield proxy.get_subsearch_result_set(result_ids, query.split(" "));
        }
        previous_query = query;
        if (result_ids.length == 0) return; // We don't have any results

        // Check if we've been cancelled
        if (cancellable.is_cancelled())
            return;

        if (result == null) {
            result = new SearchResult();
            result.app = this.app;
            result.subresults = new ListStore(typeof(SearchSubResult));
            result.launch_app.connect(() => proxy.launch_search.begin(
                previous_query.split(" "), Gdk.CURRENT_TIME));
        }
        result.ids = result_ids;

        result.subresults.remove_all();
        for (int i = 0; i < result_ids.length; i++) {
            if (i >= 3 && result_ids[i].index_of("special:") != 0) continue;
            load_meta.begin(result_ids[i], result.subresults, (obj, res) => {
                bool ok = load_meta.end(res);
                if (ok && !appended) {
                    if (result != null) // Fixes a race condition that causes a crash
                        output.insert_sorted(result, list.result_sort_func);
                    appended = true;
                }
            });
        }
    }


    public async bool load_meta(string id, ListStore output) {
        uint timestamp = Gdk.CURRENT_TIME;
        if (meta_cache == null) meta_cache = new HashTable<string, SearchSubResult>(str_hash, str_equal);
        if (meta_cache.contains(id)) {
            var obj = meta_cache.lookup(id);
            if (obj != null) // (I hope) fixes a race condition
                output.append(obj);
            return true;
        }

        SearchSubResult result = new SearchSubResult();
        HashTable<string, Variant>[] proxy_results = yield proxy.get_result_metas(new string[] { id });
        if (proxy_results.length == 0) return false;
        HashTable<string, Variant> metadata = proxy_results[0];
        if (metadata == null) return false;
        result.timestamp = timestamp;
        result.id = id;
        result.name = metadata.lookup("name").get_string();
        if (metadata.contains("description"))
            result.description = metadata.lookup("description").get_string();
        else
            result.description = null;
        if (metadata.contains("icon"))
            result.icon = Icon.deserialize(metadata.lookup("icon"));
        else if (metadata.contains("gicon"))
            result.icon = Icon.new_for_string(metadata.lookup("gicon").get_string());
        else
            result.icon = null;
        result.launch.connect(() => proxy.activate_result.begin(id, previous_query.split(" "), Gdk.CURRENT_TIME, (obj, res) => {
            proxy.activate_result.end(res);
        }));
        if (output != null) // Deal with the output getting reset
            output.append(result);

        meta_cache.insert(id, result);
        return true;
    }

    public void end_search() {
        result = null;
        appended = false;
        previous_query = null;
        meta_cache = null;
    }
}

class SearchResult : Object {
    public DesktopAppInfo app;
    public string[] ids;
    public ListStore subresults;

    public signal void launch_app();
}

class SearchSubResult : Object {
    public string id;
    public uint timestamp;

    public Icon? icon;
    public string name;
    public string? description;

    public signal void launch();
}

class SearchResultView : Gtk.FlowBoxChild {
    private Gtk.FlowBox subresult_view;

    public SearchResultView(SearchResult result) {
        this.can_focus = false;

        this.get_style_context().add_class("card");
        this.margin_start = 8;
        this.margin_end = 8;

        Gtk.Box layout = new Gtk.Box(Gtk.Orientation.VERTICAL, 6);
        this.add(layout);

        Gtk.Box btn_layout = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 8);

        Icon icon_gicon = result.app.get_icon();
        if (icon_gicon is ThemedIcon) {
            string symbolic_name = (icon_gicon as ThemedIcon).names[0] + "-symbolic";
            if (Gtk.IconTheme.get_default().has_icon(symbolic_name)) {
                Gtk.Image app_icon = new Gtk.Image.from_icon_name(symbolic_name,
                    Gtk.IconSize.INVALID);
                app_icon.pixel_size = 16;
                btn_layout.pack_start(app_icon, false, false);
            }
        }

        Gtk.Label app_name = new Gtk.Label(result.app.get_name());
        btn_layout.add(app_name);

        Gtk.Image app_arrow = new Gtk.Image.from_icon_name("go-next-symbolic",
            Gtk.IconSize.BUTTON);
        btn_layout.pack_end(app_arrow, false, false);

        Gtk.Button btn = new Gtk.Button();
        btn.add(btn_layout);
        btn.get_style_context().add_class(Gtk.STYLE_CLASS_FLAT);
        btn.clicked.connect(() => result.launch_app());

        Gtk.Box btn_size_limit = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
        btn_size_limit.pack_start(btn, false);
        layout.pack_start(btn_size_limit, false);

        subresult_view = new Gtk.FlowBox();
        subresult_view.max_children_per_line = 1;
        subresult_view.selection_mode = Gtk.SelectionMode.NONE;
        subresult_view.bind_model(result.subresults, obj => new SearchSubResultView(obj as SearchSubResult));
        layout.pack_start(subresult_view);

        this.show_all();
    }

    public void launch_first_subresult() {
        SearchSubResultView? view = subresult_view.get_child_at_index(0) as SearchSubResultView;
        if (view != null) view.result.launch();
    }
}

class SearchSubResultView : Gtk.FlowBoxChild {
    public SearchSubResult result;

    public SearchSubResultView(SearchSubResult result) {
        this.result = result;
        this.can_focus = false;

        Gtk.Button btn = new Gtk.Button();
        btn.get_style_context().add_class(Gtk.STYLE_CLASS_FLAT);
        btn.clicked.connect(() => result.launch());
        this.add(btn);

        Gtk.Box layout = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 6);
        btn.add(layout);

        if (result.icon != null) {
            Gtk.Image icon = new Gtk.Image.from_gicon(result.icon, Gtk.IconSize.INVALID);
            icon.pixel_size = 24;
            layout.pack_start(icon, false);
        }

        Gtk.Box details = new Gtk.Box(Gtk.Orientation.VERTICAL, 2);
        details.margin_start = 4;
        layout.pack_start(details);

        var name_str = result.name.split("\n")[0];
        Gtk.Label name = new Gtk.Label(Panel.fix_markup(name_str));
        name.xalign = 0;
        name.use_markup = true;
        name.wrap = true;
        name.wrap_mode = Pango.WrapMode.WORD_CHAR;
        name.lines = 2;
        name.ellipsize = Pango.EllipsizeMode.END;
        name.max_width_chars = 60;
        details.pack_start(name);

        if (result.description != null) {
            var desc_str = result.description.split("\n")[0];
            Gtk.Label desc = new Gtk.Label(Panel.fix_markup(desc_str));
            desc.xalign = 0;
            desc.use_markup = true;
            desc.wrap = true;
            desc.wrap_mode = Pango.WrapMode.WORD_CHAR;
            desc.lines = 3;
            desc.ellipsize = Pango.EllipsizeMode.END;
            desc.max_width_chars = 60;
            details.pack_start(desc);
        }

        this.show_all();
    }
}

[DBus (name = "org.gnome.Shell.SearchProvider2")]
interface DBusSearchInterface : Object {
    public async abstract string[] get_initial_result_set(string[] query);
    public async abstract string[] get_subsearch_result_set(string[] old_results, string[] new_query);
    public async abstract HashTable<string, Variant>[] get_result_metas(string[] ids);
    public async abstract void activate_result(string id, string[] query, uint timestamp);
    public async abstract void launch_search(string[] query, uint timestamp);
}
