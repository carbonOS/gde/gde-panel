public enum NetworkState {
    // Generic (explicit values for comparison)
    DISCONNECTED = 0,
    FAILED = 1,
    CONNECTING = 2,
    CONNECTED = 3,

    // VPN
    CONNECTED_VPN,
    CONNECTING_VPN,
    FAILED_VPN,

    // Wifi
    FAILED_WIFI,
    CONNECTING_WIFI,
    CONNECTED_WIFI_NONE,
    CONNECTED_WIFI_WEAK,
    CONNECTED_WIFI_OK,
    CONNECTED_WIFI_GOOD,
    CONNECTED_WIFI_EXCELLENT,

    // Mobile
    FAILED_MOBILE,
    CONNECTING_MOBILE,
    CONNECTED_MOBILE_NONE,
    CONNECTED_MOBILE_WEAK,
    CONNECTED_MOBILE_OK,
    CONNECTED_MOBILE_GOOD,
    CONNECTED_MOBILE_EXCELLENT,

    // Wired
    FAILED_WIRED,
    CONNECTING_WIRED,
    CONNECTED_WIRED;

    // Converts the state to a priority that can be used for sorting
    public int get_priority() {
        switch (this) {
            case NetworkState.DISCONNECTED:
            default:
                return 0;
            case NetworkState.FAILED_WIRED:
            case NetworkState.FAILED_WIFI:
            case NetworkState.FAILED_MOBILE:
                return 1;
            case NetworkState.CONNECTED_MOBILE_NONE:
            case NetworkState.CONNECTED_MOBILE_WEAK:
            case NetworkState.CONNECTED_MOBILE_OK:
            case NetworkState.CONNECTED_MOBILE_GOOD:
            case NetworkState.CONNECTED_MOBILE_EXCELLENT:
                return 2;
            case NetworkState.CONNECTING_MOBILE:
                return 3;
            case NetworkState.CONNECTED_WIFI_NONE:
            case NetworkState.CONNECTED_WIFI_WEAK:
            case NetworkState.CONNECTED_WIFI_OK:
            case NetworkState.CONNECTED_WIFI_GOOD:
            case NetworkState.CONNECTED_WIFI_EXCELLENT:
                return 4;
            case NetworkState.CONNECTING_WIFI:
                return 5;
            case NetworkState.CONNECTED_WIRED:
                return 6;
            case NetworkState.CONNECTING_WIRED:
                return 7;
        }
    }

    // Converts the state to an icon for display
    public string to_icon() {
        switch (this) {
            case NetworkState.DISCONNECTED:
                return "network-offline-symbolic";
            case NetworkState.CONNECTED_VPN:
                return "network-vpn-symbolic";
            case NetworkState.CONNECTING_VPN:
                return "network-vpn-acquiring-symbolic";
            case NetworkState.FAILED_VPN:
                return "network-vpn-no-route-symbolic";
            case NetworkState.FAILED_WIFI:
                return "network-wireless-offline-symbolic";
            case NetworkState.CONNECTING_WIFI:
                return "network-wireless-acquiring-symbolic";
            case NetworkState.CONNECTED_WIFI_NONE:
                return "network-wireless-signal-none-symbolic";
            case NetworkState.CONNECTED_WIFI_WEAK:
                return "network-wireless-signal-weak-symbolic";
            case NetworkState.CONNECTED_WIFI_OK:
                return "network-wireless-signal-ok-symbolic";
            case NetworkState.CONNECTED_WIFI_GOOD:
                return "network-wireless-signal-good-symbolic";
            case NetworkState.CONNECTED_WIFI_EXCELLENT:
                return "network-wireless-signal-excellent-symbolic";
            case NetworkState.FAILED_MOBILE:
                return "network-cellular-offline-symbolic";
            case NetworkState.CONNECTING_MOBILE:
                return "network-cellular-acquiring-symbolic";
            case NetworkState.CONNECTED_MOBILE_NONE:
                return "network-cellular-signal-none-symbolic";
            case NetworkState.CONNECTED_MOBILE_WEAK:
                return "network-cellular-signal-weak-symbolic";
            case NetworkState.CONNECTED_MOBILE_OK:
                return "network-cellular-signal-ok-symbolic";
            case NetworkState.CONNECTED_MOBILE_GOOD:
                return "network-cellular-signal-good-symbolic";
            case NetworkState.CONNECTED_MOBILE_EXCELLENT:
                return "network-cellular-signal-excellent-symbolic";
            case NetworkState.FAILED_WIRED:
                return "network-wired-offline-symbolic";
            case NetworkState.CONNECTING_WIRED:
                return "network-wired-acquiring-symbolic";
            case NetworkState.CONNECTED_WIRED:
                return "network-wired-symbolic";
            default:
                critical("Unknown network state; cannot select icon: %s",
                    this.to_string());
                return "network-no-route-symbolic"; // tx-rx with question mark
        }
    }

    // Used for comparison: state.to_generic() == NetworkState.CONNECTED
    public NetworkState to_generic() {
        switch (this) {
            case NetworkState.FAILED_WIRED:
            case NetworkState.FAILED_WIFI:
            case NetworkState.FAILED_MOBILE:
            case NetworkState.FAILED_VPN:
                return NetworkState.FAILED;
            case NetworkState.CONNECTING_WIRED:
            case NetworkState.CONNECTING_WIFI:
            case NetworkState.CONNECTING_MOBILE:
            case NetworkState.CONNECTING_VPN:
                return NetworkState.CONNECTING;
            case NetworkState.CONNECTED_MOBILE_NONE:
            case NetworkState.CONNECTED_MOBILE_WEAK:
            case NetworkState.CONNECTED_MOBILE_OK:
            case NetworkState.CONNECTED_MOBILE_GOOD:
            case NetworkState.CONNECTED_MOBILE_EXCELLENT:
            case NetworkState.CONNECTED_WIFI_NONE:
            case NetworkState.CONNECTED_WIFI_WEAK:
            case NetworkState.CONNECTED_WIFI_OK:
            case NetworkState.CONNECTED_WIFI_GOOD:
            case NetworkState.CONNECTED_WIFI_EXCELLENT:
            case NetworkState.CONNECTED_WIRED:
            case NetworkState.CONNECTED_VPN:
                return NetworkState.CONNECTED;
            default:
                return this;
        }
    }
}
