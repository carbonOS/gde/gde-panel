class WifiDevice : NetworkDevice {
    private NM.DeviceWifi device;

    public string? ssid {
        get; private set;
        default = null;
    }

    public bool is_hotspot {
        get; private set;
        default = false;
    }

    public WifiDevice(NM.DeviceWifi device) {
        this.device = device;

        device.notify["active-access-point"].connect(update);
        device.state_changed.connect(update);
    }

    // Matches gnome-control-center
    private NetworkState strength_to_state(uint strength) {
        if (strength >= 80)
            return NetworkState.CONNECTED_WIFI_EXCELLENT;
        if (strength >= 50)
            return NetworkState.CONNECTED_WIFI_GOOD;
        if (strength >= 40)
            return NetworkState.CONNECTED_WIFI_OK;
        if (strength >= 20)
            return NetworkState.CONNECTED_WIFI_WEAK;
        return NetworkState.CONNECTED_WIFI_NONE;
    }

    public override void update() {
        this.freeze_notify();
        NM.AccessPoint? ap = device.active_access_point;

        // Get the AP's name
        if (ap != null)
            ssid = NM.Utils.ssid_to_utf8(ap.ssid.get_data());

        is_hotspot = is_hotspot_internal();

        switch (device.state) {
            case NM.DeviceState.UNKNOWN:
            case NM.DeviceState.UNMANAGED:
            case NM.DeviceState.FAILED:
                tooltip = null;
                state = NetworkState.FAILED_WIFI;
                break;
            case NM.DeviceState.DEACTIVATING:
            case NM.DeviceState.UNAVAILABLE:
            case NM.DeviceState.DISCONNECTED:
                tooltip = null;
                state = NetworkState.DISCONNECTED;
                break;
            case NM.DeviceState.PREPARE:
            case NM.DeviceState.CONFIG:
            case NM.DeviceState.NEED_AUTH:
            case NM.DeviceState.IP_CONFIG:
            case NM.DeviceState.IP_CHECK:
            case NM.DeviceState.SECONDARIES:
                if (is_hotspot) {
                    tooltip = null;
                    state = NetworkState.DISCONNECTED;
                    break;
                }
                if (ssid == null) { // Sometimes this is true when starting up
                    tooltip = "Connecting...";
                } else {
                    tooltip = "Connecting to %s...".printf(ssid);
                }
                state = NetworkState.CONNECTING_WIFI;
                break;
            case NM.DeviceState.ACTIVATED:
                if (ap == null) { // Sometimes this is true when starting up
                    tooltip = "Connected";
                    state = NetworkState.CONNECTED_WIFI_NONE;
                } else {
                    tooltip = "Connected to %s".printf(ssid);
                    state = strength_to_state(ap.strength);
                }
                break;
        }

        this.thaw_notify();
    }

    public override bool owns_device(NM.Device dev) {
        return this.device == dev;
    }

    private bool is_hotspot_internal() {
        NM.ActiveConnection? aconn = device.active_connection;
        if (aconn == null) return false;
        NM.RemoteConnection? rconn = aconn.connection;
        if (rconn == null) return false;
        NM.SettingIPConfig setting = rconn.get_setting_ip4_config();
        if (setting == null) return false;
        return setting.method == NM.SettingIP4Config.METHOD_SHARED;
    }
}

class WifiToggle : Panel.CCToggle {
    private NM.Client client;
    private List<WifiDevice> devices;

    public WifiToggle(Panel.Signals sig, NM.Client client) {
        this.client = client;

        this.name = "Wifi";
        this.status = null;
        this.icon_name = "network-wireless-disabled-symbolic";
        this.active = client.wireless_enabled;
        this.ordering = 20;
        this.more_settings = "gnome-wifi-panel.desktop";

        this.devices = new List<WifiDevice>();

        this.bind_property("active", client, "wireless-enabled",
            BindingFlags.BIDIRECTIONAL);
        this.notify["active"].connect(update);

        // Connect to the client
        client.device_added.connect(on_device_added);
        client.device_removed.connect(on_device_removed);
        client.devices.@foreach(dev => on_device_added(dev));
        update();

        // Register with the panel
        sig.register_cc_toggle(this);
    }

    private void on_device_added(NM.Device dev) {
        // Don't handle simulated/virtual interfaces
        string iface = dev.get_iface();
        if (iface.has_prefix("vmnet") ||
            iface.has_prefix("lo") ||
            iface.has_prefix("veth") ||
            iface.has_prefix("vboxnet"))
            return;

        if (dev is NM.DeviceWifi) {
            var wifi_dev = new WifiDevice(dev as NM.DeviceWifi);
            devices.append(wifi_dev);
            wifi_dev.notify["state"].connect(update);
            wifi_dev.notify["is-hotspot"].connect(update);
        }

        foreach (var _dev in devices)
            _dev.update();
        update();
    }

    private void on_device_removed(NM.Device dev) {
        foreach (var handler in devices) {
            if (handler.owns_device(dev))
                devices.remove(handler);
        }
        update();
    }

    private void update() {
        // Only show if we have at least one device
        visible = devices.length() > 0;
        if (!visible) return;

        if (!active) {
            this.icon_name = "network-wireless-disabled-symbolic";
            this.status = null;
            return;
        }

        // Find the best Wifi device
        WifiDevice? best = null;
        var best_score = -1;
        foreach (var handler in devices) {
            var score = handler.state.get_priority();
            if (score > best_score) {
                best_score = score;
                best = handler;
            }
        }

        this.freeze_notify();
        if (best.is_hotspot) {
            icon_name = "network-wireless-offline-symbolic";
            status = "<small>Hotspot Active</small>";
        } else if (best.state < NetworkState.CONNECTING_WIFI) {
            icon_name = "network-wireless-offline-symbolic";
            status = "Disconnected";
        } else if (best.state.to_generic() < NetworkState.CONNECTED) {
            icon_name = best.state.to_icon();
            status = "<small>Connecting...</small>";
        } else {
            icon_name = best.state.to_icon();
            status = "<small>%s</small>".printf(best.ssid);
        }
        this.thaw_notify();
    }
}
