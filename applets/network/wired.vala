class WiredDevice : NetworkDevice {
    private NM.DeviceEthernet device;

    public WiredDevice(NM.DeviceEthernet device) {
        this.device = device;
        device.state_changed.connect(update);
        update();
    }

    public override void update() {
        this.freeze_notify();

        switch (device.state) {
            case NM.DeviceState.UNKNOWN:
            case NM.DeviceState.UNMANAGED:
            case NM.DeviceState.FAILED:
                tooltip = null;
                state = NetworkState.FAILED_WIRED;
                break;
            case NM.DeviceState.DEACTIVATING:
            case NM.DeviceState.UNAVAILABLE:
            case NM.DeviceState.DISCONNECTED:
                tooltip = null;
                state = NetworkState.DISCONNECTED;
                break;
            case NM.DeviceState.PREPARE:
            case NM.DeviceState.CONFIG:
            case NM.DeviceState.NEED_AUTH:
            case NM.DeviceState.IP_CONFIG:
            case NM.DeviceState.IP_CHECK:
            case NM.DeviceState.SECONDARIES:
                tooltip = "Connecting...";
                state = NetworkState.CONNECTING_WIRED;
                break;
            case NM.DeviceState.ACTIVATED:
                tooltip = "Connected";
                state = NetworkState.CONNECTED_WIRED;
                break;
        }

        this.thaw_notify();
    }

    public override bool owns_device(NM.Device dev) {
        return this.device == dev;
    }
}

