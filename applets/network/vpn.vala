class VPNIndicator : Panel.CCInfo {
    private List<NM.VpnConnection> conns;

    public VPNIndicator(Panel.Signals sig, NM.Client client) {
        this.panel_ordering = 31;
        this.icon_name = null;
        this.label = null;

        conns = new List<NM.VpnConnection>();

        client.active_connection_added.connect(on_connection_added);
        client.active_connection_removed.connect(on_connection_removed);
        client.active_connections.@foreach(conn => on_connection_added(conn));

        // Register with the panel
        sig.register_cc_info(this, "gnome-network-panel.desktop");
    }

    private void on_connection_added(NM.ActiveConnection conn) {
        if (conn is NM.VpnConnection) {
            var vpn = conn as NM.VpnConnection;
            conns.append(vpn);
            vpn.notify["vpn-state"].connect(update);
            update();
        }
    }

    private void on_connection_removed(NM.ActiveConnection conn) {
        if (conn is NM.VpnConnection) {
            conns.remove(conn as NM.VpnConnection);
            update();
        }
    }

    private void update() {
        conns.sort((CompareFunc<NM.VpnConnection>) conn_sort); // Sort by status

        var has_vpn = conns.length() > 0, // Have any vpn
            has_multi = conns.length() > 1; // Have more than one vpn
        NM.VpnConnection conn = has_vpn ? conns.nth_data(0) : null;

        this.panel_icon_name = has_vpn ? get_icon(conn) : null;
        this.panel_tooltip = has_vpn ? get_status(conn, has_multi) : null;
    }

    private int conn_sort(NM.VpnConnection a, NM.VpnConnection b) {
        return a.vpn_state - b.vpn_state;
    }

    private string? get_icon(NM.VpnConnection conn) {
        switch (conn.vpn_state) {
            case NM.VpnConnectionState.ACTIVATED:
                return "network-vpn-symbolic";
            case NM.VpnConnectionState.CONNECT:
            case NM.VpnConnectionState.IP_CONFIG_GET:
            case NM.VpnConnectionState.NEED_AUTH:
            case NM.VpnConnectionState.PREPARE:
                return "network-vpn-acquiring-symbolic";
            case NM.VpnConnectionState.FAILED:
            case NM.VpnConnectionState.DISCONNECTED:
                return null;
            case NM.VpnConnectionState.UNKNOWN:
            default:
                return "network-vpn-no-route-symbolic";
        }
    }

    private string? get_status(NM.VpnConnection conn, bool multiple) {
        switch (conn.vpn_state) {
            case NM.VpnConnectionState.ACTIVATED:
                if (multiple)
                    return "Connected to VPNs";
                else
                    return "Connected to %s".printf(conn.id);
            case NM.VpnConnectionState.CONNECT:
            case NM.VpnConnectionState.IP_CONFIG_GET:
            case NM.VpnConnectionState.NEED_AUTH:
            case NM.VpnConnectionState.PREPARE:
                if (multiple)
                    return "Connecting to VPNs...";
                else
                    return "Connecting to %s...".printf(conn.id);
            case NM.VpnConnectionState.FAILED:
            case NM.VpnConnectionState.DISCONNECTED:
            case NM.VpnConnectionState.UNKNOWN:
            default:
                return null;
        }
    }
}
