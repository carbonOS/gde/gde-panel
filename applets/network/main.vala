public abstract class NetworkDevice : GLib.Object {
    public NetworkState state {
        get; protected set;
        default = NetworkState.DISCONNECTED;
    }

    public string? tooltip {
        get; protected set;
        default = null;
    }

    public abstract void update();

    public abstract bool owns_device(NM.Device dev);
}

public class NetworkIndicator : Panel.CCInfo {
    private NM.Client client;
    private List<NetworkDevice> devices;

    private Panel.CCInfo hotspot_info;

    public NetworkIndicator(Panel.Signals sig, NM.Client client) {
        this.client = client;
        this.panel_ordering = 30;
        this.icon_name = null;
        this.label = null;
        this.devices = new List<NetworkDevice>();

        // Set up the hotspot info
        hotspot_info = new Panel.CCInfo();
        hotspot_info.panel_ordering = 31;
        hotspot_info.icon_name = hotspot_info.label = null;

        // Connect to the client
        client.device_added.connect(on_device_added);
        client.device_removed.connect(on_device_removed);
        client.devices.@foreach(dev => on_device_added(dev));

        // Register with the panel
        sig.register_cc_info(this, "");
        sig.register_cc_info(hotspot_info, "");
    }

    private void on_device_added(NM.Device dev) {
        // Don't handle simulated/virtual interfaces
        string iface = dev.get_iface();
        if (iface.has_prefix("vmnet") ||
            iface.has_prefix("lo") ||
            iface.has_prefix("veth") ||
            iface.has_prefix("vboxnet"))
            return;

        NetworkDevice? handler = null;
        if (dev is NM.DeviceWifi) {
            handler = new WifiDevice(dev as NM.DeviceWifi);
        } else if (dev is NM.DeviceEthernet) {
            handler = new WiredDevice(dev as NM.DeviceEthernet);
        } else if (dev is NM.DeviceModem) {
            handler = new CellularDevice(dev as NM.DeviceModem);
        } else {
            debug("Unknown device type: %s", dev.device_type.to_string());
        }

        if (handler != null) {
            devices.append(handler);
            handler.notify["state"].connect(update);

            if (handler is WifiDevice) // Hook up wifi signal for wireless
                handler.notify["is-hotspot"].connect(update_hotspot);
        }

        foreach (var _handler in devices)
            _handler.update();
        update();
    }

    private void on_device_removed(NM.Device dev) {
        foreach (var handler in devices) {
            if (handler.owns_device(dev))
                devices.remove(handler);
        }
        update();
    }

    private void update() {
        NetworkDevice? best = null;
        var best_score = -1;

        foreach (var handler in devices) {
            var score = handler.state.get_priority();
            if (score > best_score) {
                best_score = score;
                best = handler;
            }
        }

        // Update the main indicator
        if (best.state.to_generic() > NetworkState.DISCONNECTED) {
            this.panel_icon_name = best.state.to_icon();
            this.panel_tooltip = best.tooltip;
        } else { // Couldn't find anything connected; hide the indicator
            this.panel_icon_name = null;
            this.panel_tooltip = null;
        }

        update_hotspot();
    }

    private void update_hotspot() {
        bool has_hotspot = false;
        foreach (var handler in devices) {
            // Check to see if the handler has an active wifi hotspot
            if (handler is WifiDevice && ((WifiDevice) handler).is_hotspot) {
                has_hotspot = true;
                break;
            }
        }

        // Update the hotspot indicator
        if (has_hotspot) {
            hotspot_info.panel_icon_name = "network-wireless-hotspot-symbolic";
            hotspot_info.panel_tooltip = "Hotspot Active";
        } else {
            hotspot_info.panel_icon_name = null;
            hotspot_info.panel_tooltip = null;
        }
    }
}

public Panel.Applet? create_applet() {
    var sig = Panel.Signals.obtain();

    NM.Client.new_async.begin(null, (obj, result) => {
        NM.Client client;
        try {
            client = NM.Client.new_async.end(result);
        } catch (Error e) {
            critical("Couldn't obtain NetworkManager client: %s", e.message);
            return;
        }

        // Set up the indicators
        var main_indicator = new NetworkIndicator(sig, client);
        main_indicator.@ref(); // Don't free itself

        var vpn_indicator = new VPNIndicator(sig, client);
        vpn_indicator.@ref(); // Don't free itself

        // Set up the toggles
        var wifi_toggle = new WifiToggle(sig, client);
        wifi_toggle.@ref(); // Don't free itself
        // TODO: VPN Toggle? (Which do we turn on? Maybe only an off switch)
        // TODO: Hotspot toggle? (Technically infeasible rn)
    });

    // Open the captive portal helper if needed
    // TODO: Use an actual captive portal helper
    NetworkMonitor nmon = NetworkMonitor.get_default();
    nmon.network_changed.connect(() => {
        if (nmon.get_connectivity() == NetworkConnectivity.PORTAL) {
            var app = new DesktopAppInfo("org.gnome.Epiphany.desktop");
            Carbon.Utils.launch(app, null);
        }
    });

    return null;
}
