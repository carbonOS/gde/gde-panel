class CellularDevice : NetworkDevice {
    private NM.DeviceModem device;
    private DBusObjectManagerClient? modem_manager;

    // Adapted from:
    // https://github.com/elementary/wingpanel-indicator-network/blob/master/src/Widgets/ModemInterface.vala#L37
    enum ModemAccessTechnology {
        UNKNOWN = 0,
        POTS = 1 << 0,
        GSM = 1 << 1,
        GSM_COMPACT = 1 << 2,
        GPRS = 1 << 3,
        EDGE = 1 << 4,
        UMTS = 1 << 5,
        HSDPA = 1 << 6,
        HSUPA = 1 << 7,
        HSPA = 1 << 8,
        HSPA_PLUS = 1 << 9,
        1XRTT = 1 << 10,
        EVDO0 = 1 << 11,
        EVDOA = 1 << 12,
        EVDOB = 1 << 13,
        LTE = 1 << 14,
        ANY = 0xFFFFFFFF;

        public string? to_string() {
            switch (this) {
                case ModemAccessTechnology.UNKNOWN:
                case ModemAccessTechnology.POTS:
                case ModemAccessTechnology.ANY:
                    return null;
                case ModemAccessTechnology.GSM:
                case ModemAccessTechnology.GSM_COMPACT:
                case ModemAccessTechnology.GPRS:
                case ModemAccessTechnology.1XRTT:
                    return "G";
                case ModemAccessTechnology.EDGE:
                    return "E";
                case ModemAccessTechnology.UMTS:
                case ModemAccessTechnology.EVDO0:
                case ModemAccessTechnology.EVDOA:
                case ModemAccessTechnology.EVDOB:
                    return "3G";
                case ModemAccessTechnology.HSDPA:
                case ModemAccessTechnology.HSUPA:
                case ModemAccessTechnology.HSPA:
                    return "H";
                case ModemAccessTechnology.HSPA_PLUS:
                    return "H+";
                case ModemAccessTechnology.LTE:
                    return "LTE";
                default:
                    return null;
            }
        }
    }

    private uint32 signal_quality = 0;
    private ModemAccessTechnology access_technology =
        ModemAccessTechnology.UNKNOWN;

    public CellularDevice(NM.DeviceModem device) {
        this.device = device;
        device.state_changed.connect(update);
        update();
        obtain_modem_manager.begin();
    }

    public override void update() {
        this.freeze_notify();

        tooltip = null;
        state = NetworkState.DISCONNECTED;

        switch (device.state) {
            case NM.DeviceState.UNKNOWN:
            case NM.DeviceState.UNMANAGED:
            case NM.DeviceState.FAILED:
                tooltip = null;
                state = NetworkState.FAILED_MOBILE;
                break;
            case NM.DeviceState.DEACTIVATING:
            case NM.DeviceState.UNAVAILABLE:
            case NM.DeviceState.DISCONNECTED:
                tooltip = null;
                state = NetworkState.DISCONNECTED;
                break;
            case NM.DeviceState.PREPARE:
            case NM.DeviceState.CONFIG:
            case NM.DeviceState.NEED_AUTH:
            case NM.DeviceState.IP_CONFIG:
            case NM.DeviceState.IP_CHECK:
            case NM.DeviceState.SECONDARIES:
                tooltip = "Connecting...";
                state = NetworkState.CONNECTING_MOBILE;
                break;
            case NM.DeviceState.ACTIVATED:
                // TODO: Custom icons for the different network types??
                tooltip = access_technology.to_string() ?? "Connected";
                state = quality_to_state(signal_quality);
                break;
        }

        this.thaw_notify();
    }

    public override bool owns_device(NM.Device dev) {
        return this.device == dev;
    }

    private void on_properties_changed(Variant changed) {
        var sig = changed.lookup_value("SignalQuality", VariantType.TUPLE);
        if (sig != null) {
            uint32 quality;
            bool recent;
            sig.get("(ub)", out quality, out recent);
            signal_quality = quality;
        }

        var tech = changed.lookup_value("AccessTechnologies", VariantType.UINT32);
        if (tech != null) {
            uint32 type;
            tech.get("u", out type);
            access_technology = (ModemAccessTechnology) type;
        }

        update();
    }

    private NetworkState quality_to_state(uint32 quality) {
        if (quality >= 80)
            return NetworkState.CONNECTED_MOBILE_EXCELLENT;
        if (quality >= 50)
            return NetworkState.CONNECTED_MOBILE_GOOD;
        if (quality >= 40)
            return NetworkState.CONNECTED_MOBILE_OK;
        if (quality >= 20)
            return NetworkState.CONNECTED_MOBILE_WEAK;
        return NetworkState.CONNECTED_MOBILE_NONE;
    }

    private async void obtain_modem_manager() {
        try {
            modem_manager = yield new DBusObjectManagerClient.for_bus(
                BusType.SYSTEM, DBusObjectManagerClientFlags.NONE,
                "org.freedesktop.ModemManager1",
                "/org/freedesktop/ModemManager1", null);
        } catch (Error e) {
            warning("Unable to connect to ModemManager: %s", e.message);
            return;
        }

        modem_manager.interface_proxy_properties_changed.connect(
            (obj_proxy, iface_proxy, changed, invalidated) => {
                if (iface_proxy.g_object_path == device.get_udi())
                    on_properties_changed(changed);
            });
    }
}

