class KbdLayoutApplet : Panel.Applet {
	private Gnome.XkbInfo xkbinfo;
	private Settings settings;
	private Settings wf_settings;
	private Gtk.MenuButton btn;
	private Gtk.Box list;
	private InputMethod? current = null;

    private struct InputMethod {
        string type;
        string layout;
    }

	construct {
		applet_name = "kbd-layout";
		xkbinfo = new Gnome.XkbInfo();
        settings = new Settings("org.gnome.desktop.input-sources");
        wf_settings = new Settings("org.wayfire.section.input");

        // Set up the UI
        this.btn = Panel.create_button();
        btn.no_show_all = true;
        var popover = Panel.create_popover(this, btn);
        popover.width_request = 300;
        var layout = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);

        this.list = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
        layout.add(this.list);

        var sep = new Gtk.Separator(Gtk.Orientation.HORIZONTAL);
        sep.margin_top = sep.margin_bottom = 6;
        layout.add(sep);

        var preview_btn = new Gtk.Button.with_label("Preview Layout");
        preview_btn.get_children().data.halign = Gtk.Align.START;
        preview_btn.get_style_context().add_class("flat");
        preview_btn.clicked.connect(open_kbd_preview);
        layout.add(preview_btn);

        var settings_btn = new Gtk.Button.with_label("More Settings");
        settings_btn.get_children().data.halign = Gtk.Align.START;
        settings_btn.get_style_context().add_class("flat");
        settings_btn.clicked.connect(() => {
           Carbon.Utils.launch(new DesktopAppInfo("gnome-keyboard-panel.desktop"));
        });
        layout.add(settings_btn);

        layout.show_all();
        popover.add(layout);

        this.panel_item = btn;
        settings.changed.connect(() => update());
        update();
	}

	private void update() {
        var methods = settings.get_value("sources") as InputMethod[];

        // Handle missing current input method
        var deleted = true;
        foreach (var method in methods)
            if (method == current) {
                deleted = false;
                break;
            }
        if (current == null || deleted)
            current = methods[0];

        // Parse the current xkb info
        unowned string? display_name, short_name, xkb_layout, xkb_variant;
        xkbinfo.get_layout_info(current.layout, out display_name,
            out short_name, out xkb_layout, out xkb_variant);

        // Update the button
	    btn.label = short_name;
	    btn.tooltip_text = display_name;
	    btn.visible = methods.length > 1;

        // Update the list of elements
        foreach (var child in list.get_children())
            child.destroy();
        foreach (var method in methods) {
            if (method.type != "xkb") {
                warning("Found input method type: %s", method.type);
                continue;
            }
            var select_btn = create_xkb_btn(method.layout);
            select_btn.clicked.connect(() => select_im(method));
            list.add(select_btn);
        }
	    list.show_all();

        // Sync w/ Wayfire's settings
        wf_settings.set_string("xkb-layout", xkb_layout);
        wf_settings.set_string("xkb-variant", xkb_variant ?? "");
	}

	private Gtk.Button create_xkb_btn(string id) {
	    unowned string? display_name, short_name, xkb_layout, xkb_variant;
	    xkbinfo.get_layout_info(id, out display_name, out short_name,
	        out xkb_layout, out xkb_variant);

	    var btn = new Gtk.Button();
	    btn.get_style_context().add_class("flat");

	    var hbox = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 24);
        var lbl = new Gtk.Label(display_name);
        hbox.add(lbl);
        var short_lbl = new Gtk.Label(short_name);
        hbox.pack_end(short_lbl, false, false);
        btn.add(hbox);

        return btn;
	}

	private void select_im(InputMethod method) {
	    current = method;
	    update();
	    // TODO: Update recents
	}

	private void open_kbd_preview() {
	    unowned string? layout, variant;
        xkbinfo.get_layout_info(current.layout, null, null, out layout, out variant);

        string cmdline;
        if (variant != null)
            cmdline = "gkbd-keyboard-display -l \"%s\t%s\"".printf(layout, variant);
        else
            cmdline = "gkbd-keyboard-display -l \"%s\"".printf(layout);

        try {
            Process.spawn_command_line_async(cmdline);
        } catch (Error e) {
            warning("Failed to spawn process: %s", e.message);
        }
	}
}

public Panel.Applet? create_applet() {
	return new KbdLayoutApplet();
}
