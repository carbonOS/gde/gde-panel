using GDesktop;

private void update_toggle(Panel.CCToggle toggle, Settings settings) {
    var scheme = settings.get_enum("color-scheme");
    toggle.active = (scheme == ColorScheme.PREFER_DARK);
    toggle.icon_name = (toggle.active ? "dark" : "light") + "-mode-symbolic";
}

private void transition_screen() {
    try {
        var conn = Bus.get_sync(BusType.SESSION);
        conn.call_sync("org.gnome.Shell", "/org/gnome/Shell", "org.gnome.Shell",
            "ScreenTransition", null, null, DBusCallFlags.NONE, -1);
    } catch (Error e) {
        critical("Failed to call ScreenTransition: %s", e.message);
    }
}

public Panel.Applet? create_applet() {
    var sig = Panel.Signals.obtain();
    var settings = new Settings("org.gnome.desktop.interface");

    var toggle = new Panel.CCToggle();
    toggle.name = "Dark Mode";
    toggle.status = null;

    update_toggle(toggle, settings);
    settings.changed["color-scheme"].connect(() => {
        update_toggle(toggle, settings);
    });
    toggle.notify["active"].connect(() => {
        var new_value = toggle.active ? ColorScheme.PREFER_DARK :
                                        ColorScheme.DEFAULT;
        if (settings.get_enum("color-scheme") == new_value)
            return;

        transition_screen();
        settings.set_enum("color-scheme", new_value);
    });

    sig.register_cc_toggle(toggle);
    return null;
}
