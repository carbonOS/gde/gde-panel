# gde-panel

The Graphite Desktop Environment Panel is the main component of the
GDE desktop. The panel provides much of the core functionality of the
shell, including app launching, search, window management, notifications,
and system status/toggles.

## Building

Development is easiest on carbonOS through GNOME Builder. Simply switch
carbonOS to the devel variant:

```bash
$ updatectl switch --base=devel
[Enter your credentials at the prompt]
[Reboot]
```

and hit run in GNOME Builder. It's likely that carbonOS's included build of
gde-panel will conflict with your custom build. To work around this, you can
terminate the system's gde-panel whenver you're working on your changes:

```bash
$ systemctl --user stop gde-panel
```

Once you're done working, you can restart the system's gde-panel either by
reloading GDE (<kbd>Ctrl</kbd>+<kbd>R</kbd>, followed by `r` at the prompt)
or directly:

```bash
$ systemctl --user start gde-panel
```

