#define WAYFIRE_PLUGIN
#include <wayfire/view.hpp>
#include <wayfire/core.hpp>
#include <wayfire/workspace-manager.hpp>
#include <wayfire/signal-definitions.hpp>

class wayfire_place_window : public wayfire_plugin_t
{
    signal_callback_t created_cb;
    signal_callback_t workarea_changed_cb;
    wf_option placement_mode;

    public:
    void init(wayfire_config *config)
    {
        created_cb = [=] (signal_data *data)
        {
            auto mode = placement_mode->as_string();
            if (mode != "rand-center") return;

            auto view = get_signaled_view(data);
            if (view->role != WF_VIEW_ROLE_TOPLEVEL ||
                view->parent || view->fullscreen ||
                view->maximized)
                return;

            auto workarea = output->workspace->get_workarea();
            rand_center(view, workarea);
        };

        auto section = config->get_section("place");
        placement_mode = section->get_option("mode", "rand-center");

        output->connect_signal("map-view", &created_cb);
    }

    void rand_center(wayfire_view &view, wf_geometry workarea)
    {
        wf_geometry window = view->get_wm_geometry();
        window.x = workarea.x + (workarea.width / 2) - (window.width / 2) - (rand() % 50 - 25);
        window.y = workarea.y + (workarea.height / 2) - (window.height / 2) - (rand() % 50 - 25);
        view->move(window.x, window.y);
    }

    void fini()
    {
        output->disconnect_signal("map-view", &created_cb);
    }
};

extern "C"
{
    wayfire_plugin_t *newInstance()
    {
        return new wayfire_place_window();
    }
}
