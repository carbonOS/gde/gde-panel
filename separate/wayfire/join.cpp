#define WAYFIRE_PLUGIN
#include <wayfire/plugin.hpp>
#include <wayfire/view.hpp>
#include <wayfire/debug.hpp>
#include <wayfire/core.hpp>
#include <wayfire/workspace-manager.hpp>
#include <wayfire/signal-definitions.hpp>

class wayfire_glue_window : public wf::plugin_interface_t {
    wf::signal_callback_t map_cb;
    wf::signal_callback_t move_resize_cb;
    wf::signal_callback_t parent_cb;

    public:
    void init(wayfire_config *config) override
    {
        move_resize_cb = [=] (wf::signal_data_t *data)
        {
            auto view = get_signaled_view(data);
            move_views(view);
        };

        parent_cb = [=] (wf::signal_data_t *data)
        {
            auto view = get_signaled_view(data);
            if (!view->parent) return;
            move_views(view->parent);
        };

        output->connect_signal("view-geometry-changed", &move_resize_cb);
        //output->connect_signal("view-set-parent", &parent_cb);
    }

    void move_views(wayfire_view view) {
        wf_geometry window = view->get_wm_geometry();

        // Move children in relation to this
        for (const wayfire_view child_view : view->children) {
            if (child_view->fullscreen || child_view->maximized) continue;
            wf_geometry child = child_view->get_wm_geometry();
            int new_x = window.x + (window.width - child.width) / 2;
            int new_y = window.y + (window.height - child.height) / 2;
            if (child.x != new_x || child.y != new_y)
                child_view->move(new_x, new_y);
        }

        // Move parent in relation to this
        if (view->parent && view->is_mapped() && window.x != 0 && window.y != 0) {
            wf_geometry parent = view->parent->get_wm_geometry();
            int new_x = window.x + (window.width - parent.width) / 2;
            int new_y = window.y + (window.height - parent.height) / 2;
            if (parent.x != new_x || parent.y != new_y)
                view->parent->move(new_x, new_y);
        }
    }

    void fini() override
    {
        output->disconnect_signal("view-geometry-changed", &move_resize_cb);
        //output->disconnect_signal("view-set-parent", &parent_cb);
    }
};

DECLARE_WAYFIRE_PLUGIN(wayfire_glue_window);
