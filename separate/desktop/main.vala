namespace Desktop {
    public int main(string[] args) {
        Gtk.init(ref args);

        // Set up the dbus communications
        //Signals.obtain().register();

        // Set up the window
        new Desktop.Window().show_all();

        Gtk.main();
        return 0;
    }
}
