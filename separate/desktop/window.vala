class Desktop.Window : Gtk.Window {
    construct {
        // Setup layer shell
        GtkLayerShell.init_for_window(this);
        GtkLayerShell.set_layer(this, GtkLayerShell.Layer.BOTTOM);
        GtkLayerShell.set_namespace(this, "$unfocus"); // Ask wf to focus sanely
        GtkLayerShell.set_anchor(this, GtkLayerShell.Edge.RIGHT, true);
        GtkLayerShell.set_anchor(this, GtkLayerShell.Edge.TOP, true);
        GtkLayerShell.set_anchor(this, GtkLayerShell.Edge.BOTTOM, true);

        this.width_request = 375;
    }
}
