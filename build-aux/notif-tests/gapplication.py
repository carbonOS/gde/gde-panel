#!/usr/bin/env python3
# A test program for using notifications

import gi
gi.require_version("GLib", "2.0");
from gi.repository import GLib, Gio;

app = Gio.Application.new("sh.carbon.Unknown", Gio.ApplicationFlags.FLAGS_NONE);

def on_app_activate(app):
    # Create the standard notification
    notif1 = Gio.Notification.new(title = "Hello world!");
    notif1.set_body("Testing <bad>markup</bad> & <b>good</b> markup!");
    notif1.add_button("Quit", "app.quit");
    notif1.add_button("Replace", "app.replacenotif");

    # Create the replacement notification
    notif2 = Gio.Notification.new(title = "Replacement");
    notif2.set_body("This notification is replacing the first one. Withdrawing in 5s. Tap to quit");
    notif2.set_default_action("app.quit");

    # Add the quit action
    def on_quit_action(action, param):
        print("Quitting!")
        app.quit();
    quit_action = Gio.SimpleAction.new("quit", None);
    quit_action.connect("activate", on_quit_action);
    app.add_action(quit_action);

    # Add the replace action
    def on_replace_notif_action(action, param):
        print("Replace notif!")
        app.send_notification("notif", notif2);
        GLib.timeout_add(5000, on_timeout_expired);
    def on_timeout_expired():
        print("Withdrawing notif!");
        app.withdraw_notification("notif");
        app.quit();
        return GLib.SOURCE_REMOVE;
    replace_action = Gio.SimpleAction.new("replacenotif", None);
    replace_action.connect("activate", on_replace_notif_action);
    app.add_action(replace_action);

    # Send the notification
    app.send_notification("notif", notif1);

    # Don't quit
    app.hold();

app.connect("activate", on_app_activate);
app.run(None);
