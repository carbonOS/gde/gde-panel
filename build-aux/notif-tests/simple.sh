#!/usr/bin/env bash

notify-send "System Notif, no appid" -a "Fake System Component" -i "application-x-executable"

notify-send "System Notif, w/ appid" --hint=string:desktop-entry:org.gnome.DiskUtility

flatpak run --command=notify-send org.gnome.Builder "App Notif"

notify-send "Notification with a very long title. This title is looonngg" "It also has an obnoxiously long description, with <b>BOLD</b> and <i>Italics</i> elements too! We're really putting this to the test!" --hint=string:desktop-entry:sh.carbon.Unknown --hint=string:image-path:"file:///usr/share/pixmaps/faces/launch.jpg"

