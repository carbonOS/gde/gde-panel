project(
	'gde-panel',
	'vala', 'c',
	version: '2021.1',
	meson_version: '>= 0.50.0',
	license: 'GPL-3.0-or-later'
)

# Install gsettings schemas
meson.add_install_script('build-aux/install_schema.sh')

i18n = import('i18n')

# Include the VAAPI directory in valac's arguments
# TODO: Remove this at some point
vapi_dir = meson.current_source_dir() / 'build-aux' / 'vapi'
add_project_arguments(['--vapidir', vapi_dir], language: 'vala')

# Project default cflags
add_project_arguments([
	'-DGNOME_DESKTOP_USE_UNSTABLE_API',
	'-DG_LOG_DOMAIN="gde"',
	'-DG_LOG_USE_STRUCTURED'
], language: 'c')

# The directory where applets are installed to
applets_dir = get_option('prefix') / get_option('libdir') / 'gde-panel'

# Dependencies
wayland_client = meson.get_compiler('c').find_library('wayland-client')
glib = dependency('glib-2.0')
gmodule = dependency('gmodule-2.0')
gio = dependency('gio-unix-2.0')
gtk = dependency('gtk+-3.0')
gudev = dependency('gudev-1.0')
dazzle = dependency('libdazzle-1.0')
handy = dependency('libhandy-1')
layer_shell = dependency('gtk-layer-shell-0')
gdesktop = dependency('gnome-desktop-3.0')
gdesktopenums = meson.get_compiler('vala').find_library('gdesktopenums-3.0')
libnm = dependency('libnm')
posix = meson.get_compiler('vala').find_library('posix')

# Borrowed from:
# https://gitlab.gnome.org/GNOME/gnome-control-center/blob/master/meson.build
gvc_proj = subproject(
  'gvc',
  default_options: [
    'static=true',
    'alsa=false'
  ]
)
gvc = gvc_proj.get_variable('libgvc_dep')

# Components of the shell
subdir('data')
subdir('panel')
subdir('applets') # Must come after panel
