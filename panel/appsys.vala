namespace Panel {

    // Given a pid, gets the basename of the executable (/proc/PID/comm)
    public string get_comm_from_pid(uint32? pid) {
        if (pid == null) return "unknown"; // Fall through null pid

        try {
            var path = "/proc/%u/comm".printf(pid);
            var file = File.new_for_path(path);
            var fis = file.read();
            var dis = new DataInputStream(fis);
            var line = dis.read_line();
            return line ?? "unknown";
        } catch (Error e) {
            warning("Couldn't get comm from PID: %s", e.message);
            return "unknown";
        }
    }

    // Given an appid, checks to see if an app is running
    public bool is_dbus_app_running(string appid) {
        try {
            var conn = Bus.get_sync(BusType.SESSION);
            conn.call_sync(appid,
                           "/" + appid.replace(".", "/"),
                           "org.freedesktop.DBus.Peer",
                           "Ping",
                           null, null,
                           DBusCallFlags.NO_AUTO_START,
                           -1);
            return true; // Responded to ping; it's running
        } catch (Error e) { // Timed out
            return false;
        }
    }

    // Given a dbus sender, find the process's PID. Returns null on error
    public uint32? get_dbus_pid_from_sender(BusName sender) {
        try {
            var conn = Bus.get_sync(BusType.SESSION);
            var result = conn.call_sync("org.freedesktop.DBus",
                                        "/org/freedesktop/DBus",
                                        "org.freedesktop.DBus",
                                        "GetConnectionUnixProcessID",
                                        new Variant("(s)", sender),
                                        new VariantType("(u)"),
                                        DBusCallFlags.NONE,
                                        -1);
            uint32 pid;
            result.get_child(0, "u", out pid);
            return pid;
        } catch (Error e) {
            warning("Couldn't obtain pid from dbus sender (%s): %s",
                sender, e.message);
            return null;
        }
    }

    // Given a PID, tries to read the .flatpak-info file to find the desktop
    // app id; returns null if anything goes wrong (fallback to old behavior)
    public string? get_flatpak_appid_from_pid(uint32? pid) {
        if (pid == null) return null; // Fall through null pid

        var path = "/proc/%u/root/.flatpak-info".printf(pid);

        if (FileUtils.test(path, FileTest.IS_REGULAR)) {
            var file = new KeyFile();
            file.load_from_file(path, KeyFileFlags.NONE);
            try {
                return file.get_string("Application", "name");
            } catch {
                warning("Process drawing GUI from Flatpak runtime");
                return null;
            }
        } else return null;
    }

    // Works around some hard-to-fix appid bugs
    // Used on non-flatpak apps that report their own appid
    // Any apps that ship w/ the OS that misbehave w/ their appid should go here
    public string fix_app_id(string og) {
        // Luckily, for now all apps seem to behave
        return og;
        /*
        switch (og) {
            case "foo.bar.Baz":
                return "real.app.Id";
            default:
                return og;
        }*/
    }

}
