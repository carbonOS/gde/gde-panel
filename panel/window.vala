class Panel.Window : Gtk.Window {
    Gtk.Box content;
    Gtk.Box left_applets;
    Gtk.Box center_applets;
    Gtk.Box right_applets;
    Settings settings;
    Gtk.Revealer revealer;
    Gtk.Box root;
    Gtk.Box shadow;

    construct {
        this.get_style_context().remove_class("background");
        this.decorated = false;

        // Setup settings
        settings = new Settings("sh.carbon.gde-panel");
        settings.changed["autohide"].connect(() => invalidate_autohide());
        settings.changed["place-top"].connect(invalidate_placement);
        settings.bind("force-dark", Gtk.Settings.get_default(),
            "gtk_application_prefer_dark_theme",
            SettingsBindFlags.DEFAULT);

        // Setup layer shell
        GtkLayerShell.init_for_window(this);
        GtkLayerShell.set_layer(this, GtkLayerShell.Layer.TOP);
        GtkLayerShell.set_namespace(this, "$unfocus"); // Ask wf to focus sanely
        GtkLayerShell.set_anchor(this, GtkLayerShell.Edge.LEFT, true);
        GtkLayerShell.set_anchor(this, GtkLayerShell.Edge.RIGHT, true);

        // Setup the shadow
        root = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
        shadow = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
        shadow.get_style_context().add_class("shadow");
        root.pack_start(shadow, false, false);
        this.add(root);

        // Setup the panel's root content box
        content = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 0);
        content.get_style_context().add_class("cSH-panel");
        get_style_context().add_class("opaque"); // TODO: Read this in from app-list
        if (Environment.get_variable("XDG_SESSION_CLASS") == "greeter")
            get_style_context().add_class("greeter");

        // Setup autohide
        revealer = new Gtk.Revealer();
        this.valign = Gtk.Align.START;
        revealer.add(content);
        root.pack_start(revealer);
        var swipe_gesture = new Gtk.GestureSwipe(this);
        swipe_gesture.swipe.connect(on_swipe);
        this.set_data("swipe_gesture", swipe_gesture);

        // Setup the applet locations_
        left_applets = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 2);
        content.pack_start(left_applets, false);
        left_applets.margin = 2;
        center_applets = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 2);
        content.set_center_widget(center_applets);
        right_applets = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 2);
        content.pack_end(right_applets, false);
        right_applets.margin = 2;

        // Respond to applet or bus requests
        var sig = Signals.obtain();
        sig.on_focus_window.connect(() =>
            GtkLayerShell.set_keyboard_interactivity(this, true));
        sig.on_defocus_window.connect(() =>
            GtkLayerShell.set_keyboard_interactivity(this, false));
        sig.on_invalidate_autohide.connect(() => invalidate_autohide());
        sig.on_peek.connect(duration => {
            if (peek_timer != null) Source.remove(peek_timer);
            peek_timer = Timeout.add(duration, () => {
                peek_timer = null;
                return invalidate_autohide();
            });
            invalidate_autohide();
        });

        // Load the plugins
        var ld = PluginLoader.obtain();
        ld.load();
        foreach (var applet in ld.get_applets()) {
            applet.window = this;
            applet.window_loaded();
            var widget = applet.panel_item;
            switch (applet.placement) {
                case Gtk.Align.START:
                    left_applets.add(widget);
                    break;
                case Gtk.Align.CENTER:
                    center_applets.add(widget);
                    break;
                case Gtk.Align.END:
                    right_applets.add(widget);
                    break;
            }
        }

        show_all();
        invalidate_autohide();
        invalidate_placement();
    }

    // Auohide management

    bool dodging = false; // Currently dodging a window
    bool mouse_over = false; // The mouse is currently on the window
    uint? peek_timer = null; // The ID of an active peek timeout
    uint mouse_off_timer = -1; // The ID of an active mouse-off timeout

    public bool invalidate_autohide() {
        var autohide = settings.get_boolean("autohide");

        var applet_open = false;
        Applet[] applets = PluginLoader.obtain().get_applets();
        foreach (Applet applet in applets) {
            applet_open = applet.suppress_autohide;
            if (applet_open) break;
        }

        revealer.reveal_child = !autohide || applet_open || mouse_over || (peek_timer != null);
        var panel_height = content.get_allocated_height();
        GtkLayerShell.set_exclusive_zone(this, autohide ? 0 : panel_height);

        return Source.REMOVE; // Support Timeout.add
    }

    public override bool enter_notify_event(Gdk.EventCrossing evt) {
        if (evt.mode == Gdk.CrossingMode.NORMAL) {
            mouse_over = true;
            invalidate_autohide();
        }
        return Gdk.EVENT_PROPAGATE;
    }

    public override bool leave_notify_event(Gdk.EventCrossing evt) {
        if (evt.detail != Gdk.NotifyType.INFERIOR &&
            evt.mode == Gdk.CrossingMode.NORMAL) {
            mouse_over = false;
            int duration = settings.get_int("mouse-off-delay");
			if (mouse_off_timer != -1) Source.remove(mouse_off_timer);
			mouse_off_timer = Timeout.add (duration, () => {
			    mouse_off_timer = -1;
			    invalidate_autohide();
			    return Source.REMOVE;
			});
        }
        return Gdk.EVENT_PROPAGATE;
    }

    private void on_swipe(double vx, double vy) {
        var place_top = settings.get_boolean("place-top");
        if (place_top && vy > 0 || !place_top && vy < 0) {
            mouse_over = true;
            invalidate_autohide();
            if (mouse_off_timer != -1) Source.remove(mouse_off_timer);
            mouse_off_timer = Timeout.add(1500, () => {
                mouse_off_timer = -1;
                mouse_over = false;
                invalidate_autohide();
                return Source.REMOVE;
            });
        }
    }

    // Popover Hiding

    public override bool button_release_event(Gdk.EventButton evt) {
        void* data;
        evt.window.get_user_data(out data);
        if (data == null)
            return false;
        var widget = data as Gtk.Widget?;
        if (widget == null)
            return false;

        if (widget.get_ancestor(typeof(Gtk.Popover)) == null)
            Signals.obtain().on_widget_clicked(widget);

        return Gdk.EVENT_PROPAGATE;
    }

    public override bool focus_out_event(Gdk.EventFocus evt) {
        var sig = Signals.obtain();
        sig.on_hide_popovers("!!!");
        sig.on_defocus_window();
        return Gdk.EVENT_PROPAGATE;
    }

    // Colors

    protected override void style_updated() {
        base.style_updated();
        Gtk.StyleContext ctx = content.get_style_context();
        Gtk.Settings settings = Gtk.Settings.get_default();

        bool dark = settings.gtk_application_prefer_dark_theme;
        if (!dark) dark = settings.gtk_theme_name.has_suffix("dark");
        if (!dark) {
            Gdk.RGBA rgba;
            var is_set = ctx.lookup_color("theme_bg_color", out rgba);
            dark = is_set && rgba.red + rgba.green + rgba.blue < 1.0;
        }

        var win_ctx = this.get_style_context();
        win_ctx.remove_class(dark ? "light" : "dark");
        win_ctx.add_class(dark ? "dark" : "light");
    }

    // Placement

    private void invalidate_placement() {
        var place_top = settings.get_boolean("place-top");

        GtkLayerShell.set_anchor(this, GtkLayerShell.Edge.TOP, place_top);
        GtkLayerShell.set_anchor(this, GtkLayerShell.Edge.BOTTOM, !place_top);

        var shadow_ctx = shadow.get_style_context();
        if (place_top)
            shadow_ctx.add_class("top");
        else
            shadow_ctx.remove_class("top");
        root.set_child_packing(shadow, false, false, 0,
            place_top ? Gtk.PackType.END : Gtk.PackType.START);

        revealer.transition_type = !place_top ?
            Gtk.RevealerTransitionType.SLIDE_UP :
            Gtk.RevealerTransitionType.SLIDE_DOWN;
        /*revealer.valign = !place_top ?
            Gtk.Align.END:
            Gtk.Align.START;*/
    }
}
