public class Panel.Applet : Object {
    public Gtk.Window? window;
    public Gtk.Align placement;
    public string applet_name = "ERROR";
    public bool suppress_autohide = false;
    public bool suppress_defocus = false;
    public Gtk.Widget? panel_item = null;

    public signal void window_loaded(); // Signal sent when window is set
}

namespace Panel {
    // Connects the necessary signals to a popover to make it behave properly
    public void setup_popover(Applet applet, Gtk.Popover popover,
            bool do_toggle = true, string? toggle_name = null) {
        toggle_name = toggle_name ?? applet.applet_name;
        Settings settings = new Settings("sh.carbon.gde-panel");

        popover.modal = false;
        popover.constrain_to = Gtk.PopoverConstraint.NONE;
        popover.position = settings.get_boolean("place-top") ?
            Gtk.PositionType.BOTTOM : Gtk.PositionType.TOP;
        settings.changed["place-top"].connect(() => {
            popover.position = settings.get_boolean("place-top") ?
                Gtk.PositionType.BOTTOM : Gtk.PositionType.TOP;
        });
        popover.show.connect(() => {
            var sig = Signals.obtain();
            sig.on_hide_popovers(toggle_name);
            sig.on_focus_window();
            popover.grab_focus();

            applet.suppress_autohide = true;
            sig.on_invalidate_autohide();
        });
        popover.closed.connect(() => {
            var sig = Signals.obtain();
            if (!applet.suppress_defocus)
                sig.on_defocus_window();

            applet.suppress_autohide = false;
            sig.on_invalidate_autohide();
        });
        applet.window_loaded.connect(() => {
            applet.window.key_press_event.connect(evt => {
                if (evt.keyval == Gdk.Key.Escape) popover.popdown();
                return Gdk.EVENT_PROPAGATE;
            });
        });

        var sig = Signals.obtain();
        sig.on_hide_popovers.connect(not => {
            applet.suppress_defocus = (not != toggle_name);
            if (not != toggle_name) popover.popdown();
        });
        sig.on_widget_clicked.connect(widget => {
           if (widget != popover.relative_to) popover.popdown();
        });
        if (do_toggle)
            sig.on_toggle_popover[toggle_name].connect(() => {
                if (popover.visible)
                    popover.popdown();
                else
                    popover.popup();
            });
    }

    public Gtk.Popover create_popover(Applet applet, Gtk.MenuButton btn) {
        btn.popover = new Gtk.Popover(btn);
        btn.popover.bind_property("visible", btn, "active");
        setup_popover(applet, btn.popover);
        return btn.popover;
    }

    public Gtk.MenuButton create_button(string? tooltip = null) {
        var btn = new Gtk.MenuButton();
        btn.get_style_context().add_class(Gtk.STYLE_CLASS_FLAT);

        if (tooltip != null)
            btn.tooltip_text = tooltip;
        return btn;
    }

    public Gtk.MenuButton create_img_button(string icon_name,
        string? tooltip = null, int pixel_size = 18) {
        var btn = create_button(tooltip);

        btn.get_style_context().add_class("image-button");
        var img = new Gtk.Image.from_icon_name(icon_name, Gtk.IconSize.INVALID);
        img.pixel_size = pixel_size;
        btn.add(img);

        return btn;
    }
}

public class Panel.CCApplet : Object {
    public string? panel_icon_name {
        set; get;
        default = null;
    }

    public string? panel_text {
        set; get;
        default = null;
    }

    public string? panel_tooltip {
        set; get;
        default = null;
    }

    public int panel_ordering {
        set; get;
        default = 1000; // All the way left
    }
}

public class Panel.CCInfo : Panel.CCApplet {
    public string? icon_name {
        set; get;
        default = "content-loading-symbolic";
    }

    public string? label {
        set; get;
        default = "Info";
    }
}

public class Panel.CCCustom : Panel.CCApplet {
    public CCCustom.empty() {}

    public CCCustom(Gtk.Widget w) {
        this.widget = w;
    }

    public Gtk.Widget widget;
}

public class Panel.CCToggle : Panel.CCApplet {

    public int ordering {
        set; get;
        default = 50000; // All the way at the end
    }

    public string? icon_name {
        set; get;
        default = "content-loading-symbolic";
    }

    //TODO: Emblem?

    public bool visible {
        get; set;
        default = true;
    }

    public string name {
        get; set;
        default = "Toggle";
    }

    public bool multiline = false;

    public string? status {
        get; set;
        default = "State";
    }

    public int max_width_chars {
        get; set;
        default = 10;
    }

    public bool active {
        get; set;
        default = false;
    }

    public string? more_settings = null;
}
