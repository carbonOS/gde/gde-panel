namespace Panel {
    public int main(string[] args) {
        Gtk.init(ref args);

        // Set up the dbus communications
        Signals.obtain().register();

        // Force adw-gtk3 theme
        Gtk.Settings.get_default().gtk_theme_name = "adw-gtk3";

        // Apply custom CSS
        var css_provider = new Gtk.CssProvider();
        css_provider.load_from_resource("/gde/panel/panel.css");
        Gtk.StyleContext.add_provider_for_screen(
            Gdk.Screen.get_default(), css_provider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        );
        Gtk.IconTheme.get_default().add_resource_path("/gde/panel/icons");

        // Set up the window
        var window = new Panel.Window();

        // Find the primary monitor
        var primary_settings = new Settings("sh.carbon.gde-gnome-bridge.output");
        update_primary_display(primary_settings, window);
        primary_settings.changed["primary-gtk3"].connect(() =>
            update_primary_display(primary_settings, window));

        Gtk.main();
        return 0;
    }

    private void update_primary_display(Settings s, Panel.Window win) {
        var primary = s.get_string("primary-gtk3");
        var display = Gdk.Display.get_default();
        for (int i = 0; i < display.get_n_monitors(); i++) {
            var monitor = display.get_monitor(i);
            var compare = "%d,%d".printf(monitor.geometry.x,
                monitor.geometry.y);
            if (compare == primary) {
                GtkLayerShell.set_monitor(win, monitor);
                return;
            }
        }
    }

}
