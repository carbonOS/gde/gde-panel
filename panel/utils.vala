using Gdk;

namespace Carbon.Utils {
	public void launch (AppInfo? app, string? action = null) {
		if (app == null) return;

		// Set up the launch context
		var ctx = Gdk.Display.get_default().get_app_launch_context();
		ctx.launched.connect((info, platform_data) => {
		    // Let systemd take over management/ownership of the process
		    int pid = -1;
		    platform_data.lookup("pid", "i", &pid);
			Gnome.start_systemd_scope.begin(info.get_id(), pid, null, null, null);
		});

		// Do the launch
		if (app is DesktopAppInfo && action != null) 
			(app as DesktopAppInfo).launch_action (action, ctx);
		else try {
			app.launch (null, ctx);
		} catch {}
	}
}

namespace Panel {

    [DBus(name="org.gnome.Shell")]
    interface Shell : DBusProxy {
        public abstract async void ShowOSD(HashTable<string, Variant> props);
    }

    public async void show_shell_osd(string? icon, string? text, double level) {
        var proxy = yield Bus.get_proxy<Shell>(BusType.SESSION,
            "org.gnome.Shell", "/org/gnome/Shell");

        var props = new HashTable<string, Variant>(null, null);
        if (icon != null)
            props["icon"] = icon;
        if (text != null)
            props["label"] = text;
        if (level != -1.0)
            props["level"] = level;

        yield proxy.ShowOSD(props);
    }

    // Limited markup support that we export to external apps.
    // Supports  &amp;, &quot;, &apos;, &lt; and &gt;, escapes all other &
    // Supports <b>, <i>, and <u>, escapes all other tags
    // Adapted from gnome-shell:
    // https://gitlab.gnome.org/GNOME/gnome-shell/blob/master/js/ui/messageList.js#L22
    public string fix_markup(string? markup) {
        if (markup == null) return null;

        try {
            // Create the regex parser
            var entity_regex = new Regex("&(?!amp;|quot;|apos;|lt;|gt;)");
            var tag_regex = new Regex("<(?!\\/?[biu]>)");

            // Do the replacement
            var text = entity_regex.replace(markup, markup.length, 0, "&amp;");
            text = tag_regex.replace(text, text.length, 0, "&lt;");

            // Have pango try to parse the markup
            // If markup is invalid, it throws an error, which we catch
            // and fall back to escaping everything
            Pango.parse_markup(text, -1, 0, null, null, null);

            return text;
        } catch (Error e) {
            warning("Couldn't fix markup: %s", e.message);
            return Markup.escape_text(markup); // Just escape everything
        }
    }

    // TODO: Decide if we should keep this API

    public Gdk.RGBA extract_app_color(DesktopAppInfo app) {
        var gicon = app.get_icon();
        if (gicon is Gdk.Pixbuf) {
            return extract_dominant_color(gicon as Gdk.Pixbuf);
        } else if (gicon is FileIcon) {
            string path = (gicon as FileIcon).get_file().get_path();
            var pixbuf = new Gdk.Pixbuf.from_file(path);
            if (pixbuf != null)
                return extract_dominant_color(pixbuf);
        } else {
            var info = Gtk.IconTheme.get_default().lookup_by_gicon(gicon, 64,
                0);
            if (info != null)
                return extract_dominant_color(info.load_icon());
        }

        // Fallback to white`
        return { 255, 255, 255, 1.0 };
    }

    // Implementation adapted from Canonical's Unity7
    // https://bazaar.launchpad.net/~unity-team/unity/trunk/view/head:/launcher/LauncherIcon.cpp
    public Gdk.RGBA extract_dominant_color(Gdk.Pixbuf pixbuf) {
        long rTotal = 0, gTotal = 0, bTotal = 0;
        double total = 0.0;

        uint8[] pixels = pixbuf.get_pixels_with_length();

        for (var offset = 0; offset < pixels.length; offset += 4) {
            uint8 r = pixels[offset];
            uint8 g = pixels[offset + 1];
            uint8 b = pixels[offset + 2];
            uint8 a = pixels[offset + 3];

            uint8 max = uint8.max(uint8.max(r, g), b);
            uint8 min = uint8.min(uint8.min(r, g), b);
            double saturation = (max - min) / 255.0f;
            double relevance = 0.1 + 0.9 * (a / 255.0f) * saturation;

            rTotal += (uint8) (r * relevance);
            gTotal += (uint8) (g * relevance);
            bTotal += (uint8) (b * relevance);
            total += (relevance * 255);
        }

        double r = rTotal / total, g = gTotal / total, b = bTotal / total;
        double h, s, v;

        Gtk.rgb_to_hsv(r / 255.0, g / 255.0, b / 255.0, out h, out s, out v);

        if (s > 0.15f)
            s = 0.65f;
        v = 1;

        Gtk.HSV.to_rgb(h, s, v, out r, out g, out b);

        return { r, g, b, 1.0 };
    }
}
