public class Panel.PluginLoader : Object {
	private static Once<PluginLoader> _inst;
	public static PluginLoader obtain() {
		return _inst.once(() => new PluginLoader());
	}

	private delegate Applet? CreateAppletFunc(bool in_login);
	private Applet[] applets = {};
	public Applet[] get_applets() {
	    return applets;
	}

	public void load(bool in_login = false) {
		if (!Module.supported())
		    error("Applets are unsupported on this system\n");

	    if (false /*in_login*/) {
            Settings settings = new Settings("sh.carbon.shell.lock");
            foreach (string plugin in settings.get_strv("plugins"))
                load_from_name(plugin, Gtk.Align.END, true);
	    } else {
		    Settings settings = new Settings("sh.carbon.gde-panel");
            foreach (string plugin in settings.get_strv("left-applets"))
                load_from_name(plugin, Gtk.Align.START, false);
            foreach (string plugin in settings.get_strv("center-applets"))
                load_from_name(plugin, Gtk.Align.CENTER, false);
            foreach (string plugin in settings.get_strv("right-applets"))
                load_from_name(plugin, Gtk.Align.END, false);
            foreach (string plugin in settings.get_strv("cc-applets"))
                load_from_name(plugin, Gtk.Align.END, false);
        }
	}

	private void load_from_name(string name, Gtk.Align placement, bool in_login) {
	    if (name.has_prefix("/"))
	        load_from_path(name, placement, in_login);
        else
            load_from_path(Module.build_path(BuildConfig.APPLETS_DIR, name), placement, in_login);
	}

	private void load_from_path(string path, Gtk.Align placement, bool in_login) {
		if (!path.has_suffix(Module.SUFFIX)) return; // Invalid file type

		// Load the module
		Module? module = Module.open(path, ModuleFlags.BIND_LAZY);
		if (module == null) return;

		// Create the applet
		void* init_func;
		if (!module.symbol("create_applet", out init_func)) return;
		if (init_func == null) return;
		Applet? applet = ((CreateAppletFunc) init_func)(in_login);
		module.make_resident(); // Don't unload immediately
		if (applet == null) return;

		// Process the applet
		applet.placement = placement;
		applets += applet;
	}
}
