- Panel (Overall application)
    - Go opaque on maximize
        - wf plugin that tells panel it should go opaque
        - dbus api to tell panel to go opaque
    - Redo Signals interface
        - Drop all the string stuff (artifact of extensibility)
        - Bind the applet object directly to the window, instead of a Signals singleton
    - Remove applet libraries and compile directly into exec instead
    - Use less markup and more pango attrs (fixes some warnings about escaping)

- Control Center
    - Impl toggle rearranging
        - Should probably go into g-c-c fork

- App-list
    - Some apps don't show up (for some reason?? Like glimpse)

- Detect GNOME Builder at launch, and handle log domains:
    - Do we even need this anymore?
    - Old bash script for doing this:
    
```bash
GDE_LOG_APPLETS=""
for dir in `find applets -mindepth 1 -maxdepth 1 -type d`; do
  applet=$(echo $dir | sed 's|applets/|applet-|')
  GDE_LOG_APPLETS="$GDE_LOG_APPLETS gde-$applet"
done
GDE_LOG_DOMAINS="gde$GDE_LOG_APPLETS"
[ -z "$G_MESSAGES_DEBUG" ] || GDE_LOG_DOMAINS="$G_MESSAGES_DEBUG"
```

---

- Overview (MIGRATE TO ANOTHER REPO)
    - Reimpl for modern wayfire
    - Wire up button -> open
    - Handle workspace switching
    - Handle window dragging
    - Smarter layout logic

- wf-config (MIGRATE TO ANOTHER REPO)
    - Deal w/ keyboard shortcuts from g-c-c
